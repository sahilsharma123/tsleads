package com.tslwebreg.tsleadsmobilecheckin;

import android.support.annotation.NonNull;

import java.io.Serializable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class RegistrantModel implements Serializable {

    public RegistrantModel(){

    }
    public int lEventID;
    @NonNull
    public String lRegID;
    public String lGuestID;
    public String lRegType;
    public String sPrefix;
    public String sFirstName;
    public String sMiddleName;
    public String sLastName;
    public String sSuffix;
    public String sCredentials;
    public String sTitle;
    public String sCompany;
    public String sAddress1;
    public String sAddress2;
    public String sAddress3;
    public String sCity;
    public String sState;
    public String sZip;
    public String sCountry;
    public String sPhone;
    public String sCell;
    public String sFax;
    public String sEmail;
    public String nStatus;
    public String sOther1;
    public String sOther2;
    public String sOther3;
    public String sOther4;
    public String sOther5;
    public String sOther6;
    public String sOther7;
    public String sOther8;
    public String sOther9;
    public String sOther10;
    public String dtUpdatedOn;

    @NonNull
    @PrimaryKey
    public int uniqueID;

    public int getUniqueId() {
        return uniqueID;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueID = uniqueId;
    }

    public int getlEventID() {
        return lEventID;
    }

    public void setlEventID(int lEventID) {
        this.lEventID = lEventID;
    }

    public String getlRegID() {
        return lRegID;
    }

    public void setlRegID(String lRegID) {
        this.lRegID = lRegID;
    }

    public String getlGuestID() {
        return lGuestID;
    }

    public void setlGuestID(String lGuestID) {
        this.lGuestID = lGuestID;
    }

    public String getlRegType() {
        return lRegType;
    }

    public void setlRegType(String lRegType) {
        this.lRegType = lRegType;
    }

    public String getsPrefix() {
        return sPrefix;
    }

    public void setsPrefix(String sPrefix) {
        this.sPrefix = sPrefix;
    }

    public String getsFirstName() {
        return sFirstName;
    }

    public void setsFirstName(String sFirstName) {
        this.sFirstName = sFirstName;
    }

    public String getsMiddleName() {
        return sMiddleName;
    }

    public void setsMiddleName(String sMiddleName) {
        this.sMiddleName = sMiddleName;
    }

    public String getsLastName() {
        return sLastName;
    }

    public void setsLastName(String sLastName) {
        this.sLastName = sLastName;
    }

    public String getsSuffix() {
        return sSuffix;
    }

    public void setsSuffix(String sSuffix) {
        this.sSuffix = sSuffix;
    }

    public String getsCredentials() {
        return sCredentials;
    }

    public void setsCredentials(String sCredentials) {
        this.sCredentials = sCredentials;
    }

    public String getsTitle() {
        return sTitle;
    }

    public void setsTitle(String sTitle) {
        this.sTitle = sTitle;
    }

    public String getsCompany() {
        return sCompany;
    }

    public void setsCompany(String sCompany) {
        this.sCompany = sCompany;
    }

    public String getsAddress1() {
        return sAddress1;
    }

    public void setsAddress1(String sAddress1) {
        this.sAddress1 = sAddress1;
    }

    public String getsAddress2() {
        return sAddress2;
    }

    public void setsAddress2(String sAddress2) {
        this.sAddress2 = sAddress2;
    }

    public String getsAddress3() {
        return sAddress3;
    }

    public void setsAddress3(String sAddress3) {
        this.sAddress3 = sAddress3;
    }

    public String getsCity() {
        return sCity;
    }

    public void setsCity(String sCity) {
        this.sCity = sCity;
    }

    public String getsState() {
        return sState;
    }

    public void setsState(String sState) {
        this.sState = sState;
    }

    public String getsZip() {
        return sZip;
    }

    public void setsZip(String sZip) {
        this.sZip = sZip;
    }

    public String getsCountry() {
        return sCountry;
    }

    public void setsCountry(String sCountry) {
        this.sCountry = sCountry;
    }

    public String getsPhone() {
        return sPhone;
    }

    public void setsPhone(String sPhone) {
        this.sPhone = sPhone;
    }

    public String getsCell() {
        return sCell;
    }

    public void setsCell(String sCell) {
        this.sCell = sCell;
    }

    public String getsFax() {
        return sFax;
    }

    public void setsFax(String sFax) {
        this.sFax = sFax;
    }

    public String getsEmail() {
        return sEmail;
    }

    public void setsEmail(String sEmail) {
        this.sEmail = sEmail;
    }

    public String getnStatus() {
        return nStatus;
    }

    public void setnStatus(String nStatus) {
        this.nStatus = nStatus;
    }

    public String getsOther1() {
        return sOther1;
    }

    public void setsOther1(String sOther1) {
        this.sOther1 = sOther1;
    }

    public String getsOther2() {
        return sOther2;
    }

    public void setsOther2(String sOther2) {
        this.sOther2 = sOther2;
    }

    public String getsOther3() {
        return sOther3;
    }

    public void setsOther3(String sOther3) {
        this.sOther3 = sOther3;
    }

    public String getsOther4() {
        return sOther4;
    }

    public void setsOther4(String sOther4) {
        this.sOther4 = sOther4;
    }

    public String getsOther5() {
        return sOther5;
    }

    public void setsOther5(String sOther5) {
        this.sOther5 = sOther5;
    }

    public String getsOther6() {
        return sOther6;
    }

    public void setsOther6(String sOther6) {
        this.sOther6 = sOther6;
    }

    public String getsOther7() {
        return sOther7;
    }

    public void setsOther7(String sOther7) {
        this.sOther7 = sOther7;
    }

    public String getsOther8() {
        return sOther8;
    }

    public void setsOther8(String sOther8) {
        this.sOther8 = sOther8;
    }

    public String getsOther9() {
        return sOther9;
    }

    public void setsOther9(String sOther9) {
        this.sOther9 = sOther9;
    }

    public String getsOther10() {
        return sOther10;
    }

    public void setsOther10(String sOther10) {
        this.sOther10 = sOther10;
    }

    public String getDtUpdatedOn() {
        return dtUpdatedOn;
    }

    public void setDtUpdatedOn(String dtUpdatedOn) {
        this.dtUpdatedOn = dtUpdatedOn;
    }
}
