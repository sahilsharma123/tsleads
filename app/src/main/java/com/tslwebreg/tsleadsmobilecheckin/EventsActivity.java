package com.tslwebreg.tsleadsmobilecheckin;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.content.Context;

public class EventsActivity extends AppCompatActivity {
    ListView lv;
    Global global;
    Context context = this;
    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.tslwebreg.tsleadsmobilecheckin.R.layout.activity_events);
        Toolbar toolbar = (Toolbar) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.toolbar);
        setSupportActionBar(toolbar);

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        global=((Global)getApplicationContext());

        SharedPreferences sharedPrefs = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        global.printerIP = sharedPrefs.getString("printerIP", "missing");
        if (global.printerIP == "missing"){
            editor.putString("printerIP", "192.168.1.74");
            editor.commit();
            global.printerIP = sharedPrefs.getString("printerIP", "missing");
        }
        global.printerPort = sharedPrefs.getString("printerPort", "missing");
        if (global.printerPort == "missing"){
            editor.putString("printerPort", "9100");
            editor.commit();
            global.printerPort = sharedPrefs.getString("printerPort", "missing");
        }

        global.printerMAC = sharedPrefs.getString("printerMAC", "missing");
        if (global.printerMAC == "missing"){
            //editor.putString("printerMAC", "00:17:E9:A1:83:0F");
            editor.putString("printerMAC", "98:07:2D:C8:D7:13");
            editor.commit();
            global.printerMAC = sharedPrefs.getString("printerMAC", "missing");
        }

        global.timerSearch = sharedPrefs.getString("timerSearch", "60");
        global.timerPrint = sharedPrefs.getString("timerPrint", "10");

        global.isAutoPrint=sharedPrefs.getBoolean("isAutoPrint",false);


        lv = (ListView) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.listViewEvents);

        //use this adapter to display info and remove buttons
        MyCustomAdapterListEvents adapter = new MyCustomAdapterListEvents(global.eventsList, this, this);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
                global._event_current_index = pos;
                startActivity(new Intent(context, Step1Activity.class));
            }
        });

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    1);
        }

        global.mainContext = context;
        startService(new Intent(this,ServiceAutoSync.class));

        getSupportActionBar().setIcon(com.tslwebreg.tsleadsmobilecheckin.R.mipmap.ic_launcher);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the                 // camera-related task you need to do.
                } else {
                    // permission denied, boo! Disable the                 // functionality that depends on this permission.             }
                    break;
                }
        }
    }

    @Override
    public void onRestart(){
        super.onRestart();

        //to refresh the list. Need that after removing an event
        lv = (ListView) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.listViewEvents);
        lv.invalidateViews();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.tslwebreg.tsleadsmobilecheckin.R.menu.menu_events, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.miAddEvent) {
            startActivity(new Intent(EventsActivity.this, AddEventActivity.class));
            return true;
        }
        else if (id == R.id.miSettings) {
            startActivity(new Intent(EventsActivity.this, SettingsActivity.class));
            return true;
        }
        else if (id == com.tslwebreg.tsleadsmobilecheckin.R.id.miSync) {
           //check if connected to internet
            if (global.isNetworkConnected(context)) {
                final ProgressDialog progress=new ProgressDialog(EventsActivity.this);
                progress.setMessage("Syncing Data");
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setIndeterminate(true);
                progress.setCancelable(false);
                progress.setProgress(0);
                progress.getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );
                progress.show();
                final int totalProgressTime = 100;
                final Thread t = new Thread() {
                    @Override
                    public void run() {
                        progress.setProgress(2);
                        global.bAutoSyncRunning = 1;
                        global.getEvents(context);
                        global.bAutoSyncRunning = 0;
                        progress.dismiss();
                    }
                };
                t.start();
            }
            else{
                new AlertDialog.Builder(context)
                        .setTitle("Sync")
                        .setMessage("You are not connected to the internet or your internet connection is too slow.")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
