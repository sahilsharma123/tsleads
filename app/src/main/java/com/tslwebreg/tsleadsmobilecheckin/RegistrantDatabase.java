package com.tslwebreg.tsleadsmobilecheckin;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

/**
 * Created by sahilsharma on 23/7/18.
 */

@Database(entities = {RegistrantModel.class}, version = 1, exportSchema = false)

public abstract class RegistrantDatabase extends RoomDatabase {

    public abstract RegistrantDao getRegistrantDao();

    private static RegistrantDatabase noteDB;

    public static RegistrantDatabase getInstance(Context context) {
        if (null == noteDB) {
            noteDB = buildDatabaseInstance(context);
        }
        return noteDB;
    }

//    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            // Since we didn't alter the table, there's nothing else to do here.
//        }
//    };
//
//    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            // Since we didn't alter the table, there's nothing else to do here.
//        }
//    };
//
//    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            // Since we didn't alter the table, there's nothing else to do here.
//        }
//    };

    private static RegistrantDatabase buildDatabaseInstance(Context context) {
        return Room.databaseBuilder(context,
                RegistrantDatabase.class,
                "RegistrantsDatabase")//.addMigrations(MIGRATION_3_4)
                .allowMainThreadQueries().build();
    }
}
