package com.tslwebreg.tsleadsmobilecheckin;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import java.util.List;

public class CheckBoxAdapter extends RecyclerView.Adapter<CheckBoxAdapter.ViewHolder> {

    Context context;
    ListProgramsActivity.RecordEvent recordEvent;
    CheckBoxAdapter.checkboxselectlistener chilclicklistener;
    List<ModelAnswer> modelAnswerList;

    public CheckBoxAdapter(Context context, ListProgramsActivity.RecordEvent recordEvent, List<ModelAnswer> modelAnswerList, CheckBoxAdapter.checkboxselectlistener chilclicklistener) {
        this.context     = context;
        this.recordEvent = recordEvent;
        this.chilclicklistener = chilclicklistener;
        this.modelAnswerList   = modelAnswerList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.checkbox_item, viewGroup, false);
        return new CheckBoxAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        for (ModelAnswer answer:modelAnswerList ) {
            AppCompatCheckBox appCompatCheckBox = new AppCompatCheckBox(context);
            appCompatCheckBox.setText(answer.getsAnswer());
            viewHolder.checkboxLayout.addView(appCompatCheckBox);
            viewHolder.checkboxLayout.invalidate();

            if(recordEvent.getsAnswer().equals(answer.getsAnswer())){
                appCompatCheckBox.setChecked(true);
                recordEvent.setsAnswer(recordEvent.getsAnswer());
            }

            appCompatCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    if (b) {
                        String answer =  compoundButton.getText().toString();
                        recordEvent.setsAnswer(answer);
                        recordEvent.setChecked(true);
                        chilclicklistener.onCheckboxClick(recordEvent, answer);
                    }else{
                        recordEvent.setsAnswer("");
                        recordEvent.setChecked(false);
                        chilclicklistener.onCheckboxClick(recordEvent, "");
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public interface checkboxselectlistener {
        void onCheckboxClick(ListProgramsActivity.RecordEvent event, String answer);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout checkboxLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            checkboxLayout = (LinearLayout) itemView.findViewById(R.id.checkBoxLayout);

        }
    }
}
