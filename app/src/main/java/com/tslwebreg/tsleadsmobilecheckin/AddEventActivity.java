package com.tslwebreg.tsleadsmobilecheckin;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AddEventActivity extends AppCompatActivity {
    final Context context = this;
    Global global;
    int lEventID = 0;
    String resultMain = "";
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                if (resultMain != "success") {
                    new AlertDialog.Builder(context)
                            .setTitle("Add Event")
                            .setMessage(resultMain)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.tslwebreg.tsleadsmobilecheckin.R.layout.activity_add_event);
        Toolbar toolbar = (Toolbar) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.toolbar);
        setSupportActionBar(toolbar);
        global = ((Global) getApplicationContext());
        global = ((Global) getApplicationContext());

        StrictMode.ThreadPolicy policy = new StrictMode.
                ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        TextView txtEventID = (TextView) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtEventID);
        txtEventID.requestFocus();

        Button btnCancel = (Button) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button btnAdd = (Button) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.btnAddPAC);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (global.isNetworkConnected(context) == false) {
                    new AlertDialog.Builder(context)
                            .setTitle("Add Event")
                            .setMessage("You are not connected to the internet or your internet connection is too slow.")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                } else {
                    //check if at least 1 character
                    TextView txtEventCode = (TextView) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtEventID);
                    if (txtEventCode.length() == 0) {
                        new AlertDialog.Builder(context)
                                .setTitle("Add Event")
                                .setMessage("Please enter an event code.")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    } else {
                        final ProgressDialog progress = new ProgressDialog(AddEventActivity.this);
                        progress.setMessage("Adding event");
                        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progress.setIndeterminate(true);
                        progress.setCancelable(false);
                        progress.setProgress(0);
                        progress.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        progress.show();

                        final int totalProgressTime = 100;
                        final Thread t = new Thread() {
                            @Override
                            public void run() {
                                global.bAddingNewEvent = 1;
                                progress.setProgress(2);
                                resultMain = getEventInfo();
                                if (resultMain == "success") {
                                    progress.setProgress(100);
                                    global.bAddingNewEvent = 0;
                                    finish();
                                } else {
                                    Message msg = Message.obtain();
                                    msg.what = 1;
                                    global.bAddingNewEvent = 0;
                                    handler.sendMessage(msg);
                                    progress.dismiss();
                                }
                            }
                        };
                        t.start();
                    }
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private String getEventInfo() {
        String result = "";

        TextView txtEventCode = (TextView) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtEventID);

        URL url;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL("http://www.tslwebreg.com/tslcheckin/getEventInfoForTSLCheckIn.php?access_code=" + txtEventCode.getText());
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(10000);
            urlConnection.setRequestMethod("GET");
            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);
            if (in != null) {
                result = global.convertInputStreamToString(in);
            } else {
                return "success";
            }
            try {
                JSONArray jsonArray = new JSONArray(result);
                Log.e("@@Event details", "-----" + result);
                int len1 = jsonArray.length();
                if (len1 == 0) {
                    return "Event code not found";
                } else {
                    for (int i = 0, count = len1; i < count; i++) {
                        try {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            //extract data
                            int eventIDTemp = jsonObject.getInt("lEventID");
                            lEventID = eventIDTemp;
                            String eventNameTemp = jsonObject.getString("sName");
                            String eventLocationTemp = jsonObject.getString("sLocation");
                            String eventStartDateTemp = jsonObject.getString("dtStart");
                            String eventEndDateTemp = jsonObject.getString("dtEnd");
                            int eventAccountIDTemp = jsonObject.getInt("lAccountID");

                            String eventHeader1Temp = jsonObject.getString("sHeader1");
                            String eventHeader2Temp = jsonObject.getString("sHeader2");
                            String eventHeader3Temp = jsonObject.getString("sHeader3");
                            String eventHeader4Temp = jsonObject.getString("sHeader4");
                            String eventPictureTemp = jsonObject.getString("sPicture");
                            Log.e("TAG", "url url imagee  ==== " + eventPictureTemp);

                            int eventLastNameSearchOnTemp = jsonObject.getInt("bLastNameSearchOn");
                            int eventEmailSearchOnTemp = jsonObject.getInt("bEmailSearchOn");
                            int eventScanOnTemp = jsonObject.getInt("bScanOn");

                            int alreadyThere = 0;

                            //check if already exists
                            Cursor cursor = global.mainDB.rawQuery("SELECT lEventID FROM Events Where lEventID = " + eventIDTemp, null);
                            if (cursor.moveToFirst()) {
                                while (!cursor.isAfterLast()) {
                                    alreadyThere = 1;
                                    cursor.moveToNext();
                                }
                            }
                            cursor.close();

                            //serialNumber
                            String result1 = global.saveDeviceSerialNumber(eventIDTemp, eventAccountIDTemp, global.devSerial);
                            if (result1.contains("Success") == false) {
                                if (urlConnection != null) {
                                    urlConnection.disconnect();
                                }
                                return result1;
                            }


                            if (alreadyThere == 1) {
                                if (urlConnection != null) {
                                    urlConnection.disconnect();
                                }
                                return "This event was already added.";
                            } else {
                                //add to array
                                Event event = new Event();
                                event.set_eventID(eventIDTemp);
                                event.set_eventName(eventNameTemp);
                                event.set_eventStart(eventStartDateTemp);
                                event.set_eventEnd(eventEndDateTemp);
                                event.set_eventLocation(eventLocationTemp);
                                event.set_eventAccountID(eventAccountIDTemp);
                                event.set_eventLastSync("2012-12-30 12:00:00");
                                event.set_deviceSerialnumber(global.devSerial);

                                event.set_eventHeader1(eventHeader1Temp);
                                event.set_eventHeader2(eventHeader2Temp);
                                event.set_eventHeader3(eventHeader3Temp);
                                event.set_eventHeader4(eventHeader4Temp);
                                event.set_eventPicture(eventPictureTemp);
                                event.set_eventLastNameSearchOn(eventLastNameSearchOnTemp);
                                event.set_eventEmailSearchOn(eventEmailSearchOnTemp);
                                event.set_eventScanOn(eventScanOnTemp);
                                global.eventsList.add(event);

                                //add to database
                                String sql = "";
                                sql = "INSERT INTO `Events`(`lEventID`, `sName`, `sLocation`, `sStartDate`, `sEndDate`, `lAccountID`, sDeviceSerialNumber, dtLastSync, sHeader1, sHeader2, sHeader3, sHeader4, sPicture, bLastNameSearchOn, bEmailSearchOn, bScanOn) VALUES (";
                                sql += eventIDTemp + ",";
                                sql += "'" + eventNameTemp + "',";
                                sql += "'" + eventLocationTemp + "',";
                                sql += "'" + eventStartDateTemp + "',";
                                sql += "'" + eventEndDateTemp + "',";
                                sql += eventAccountIDTemp + ",";
                                sql += "'" + global.devSerial + "',";
                                sql += "'2012-12-30 12:00:00',";
                                sql += '"' + eventHeader1Temp + '"' + ",";
                                sql += '"' + eventHeader2Temp + '"' + ",";
                                sql += '"' + eventHeader3Temp + '"' + ",";
                                sql += '"' + eventHeader4Temp + '"' + ",";
                                sql += '"' + eventPictureTemp + '"' + ",";
                                sql += eventLastNameSearchOnTemp + ",";
                                sql += eventEmailSearchOnTemp + ",";
                                sql += eventScanOnTemp + ")";

                                try {
                                    global.mainDB.execSQL(sql);
                                } catch (android.database.sqlite.SQLiteException ex) {
                                    global.appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
                                }

                                String imageURL = "";
                                if (eventPictureTemp.trim() != "") {
                                    //download picture
                                    imageURL = "http://www.tslwebreg.com/tslcheckin/images/" + eventPictureTemp;
                                    Bitmap bitmap = null;
                                    try {
                                        InputStream input = new java.net.URL(imageURL).openStream();
                                        bitmap = BitmapFactory.decodeStream(input);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        File pictureFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                                        File filePicture = new File(pictureFolder, eventPictureTemp);
                                        FileOutputStream fos = new FileOutputStream(filePicture);
                                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                                        if (fos != null) {
                                            //Toast saved = Toast.makeText(getApplicationContext(), "Image saved.", Toast.LENGTH_SHORT);/
                                        } else {
                                            //Toast unsaved = Toast.makeText(getApplicationContext(), "Image not save.", Toast.LENGTH_SHORT);/
                                        }
                                        fos.close();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                String returnVal2 = global.getAllRecordsFromOnline(context, eventIDTemp, eventAccountIDTemp, global.devSerial, 1, "");
                                if (returnVal2.equalsIgnoreCase("failed")) { //Retry if issue Oct 25 2016
                                    returnVal2 = global.getAllRecordsFromOnline(context, eventIDTemp, eventAccountIDTemp, global.devSerial, 1, "");
                                }

                                String resultHere = "";
                                if (returnVal2.equalsIgnoreCase("failed")) {
                                    resultHere = "WARNING! The event was not completely installed on your device. Your internet connection is probably intermittent. Before scanning for this event, it is recommended to find a more stable internet connection, remove the event and add the event again.";
                                } else {
                                    boolean returnVal = global.updateLastSync(0, eventIDTemp, "");
                                    resultHere = "success";
                                }

                                if (urlConnection != null) {
                                    urlConnection.disconnect();
                                }
                                return resultHere;
                            }
                        } catch (JSONException e) {
                            if (urlConnection != null) {
                                urlConnection.disconnect();
                            }
                            return "Unable to add event." + e.toString();
                        }
                    }
                }
            } catch (JSONException e) {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                return "Unable to add event." + e.toString();
            }


        } catch (Exception e) {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return "Unable to add event." + e.toString();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return "success";
    }
}
