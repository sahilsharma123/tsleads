package com.tslwebreg.tsleadsmobilecheckin;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by fdemers on 12/23/2016.
 */
public class ServiceAutoSync extends Service {
    Global global;
    private Timer timer = new Timer();

    public void onCreate() {
        super.onCreate();
        startservice();
        global=((Global)getApplicationContext());
    }

    public IBinder onBind(Intent intent) {
        Log.e("Service", "onBind");
        return null;
    }

    private void startservice() {
        timer.scheduleAtFixedRate( new TimerTask() {
            public void run() {
                new LongOperation().execute("");
                /*final Thread t = new Thread() {
                    @Override
                    public void run() {
                        if (global.bAutoSyncRunning == 0) {
                            if (global.isNetworkConnected(global.mainContext)) {
                                global.bAutoSyncRunning = 1;
                                global.getEvents(global.mainContext);
                                global.bAutoSyncRunning = 0;
                            }
                        }
                    }
                };
                t.start();*/
            }
        }, 60000, 60000);
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            if (global.bAutoSyncRunning == 0 && global.bAddingNewEvent == 0) {
                if (global.isNetworkConnected(global.mainContext)) {
                    global.bAutoSyncRunning = 1;
                    global.getEvents(global.mainContext);
                    global.bAutoSyncRunning = 0;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private void stopservice() {
        if (timer != null){
            timer.cancel();
        }
    }
}
