package com.tslwebreg.tsleadsmobilecheckin;

/**
 * Created by fdemers on 2/2/2017.
 */

public class ModelAnswer {

    private String lAnswerID;
    private String sAnswer;

    public String getlAnswerID() {
        return lAnswerID;
    }

    public void setlAnswerID(String lAnswerID) {
        this.lAnswerID = lAnswerID;
    }

    public String getsAnswer() {
        return sAnswer;
    }

    public void setsAnswer(String sAnswer) {
        this.sAnswer = sAnswer;
    }
}
