package com.tslwebreg.tsleadsmobilecheckin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fdemers on 2/2/2017.
 */

public class MyLayoutListRecords extends BaseAdapter implements ListAdapter {
    private List<RegistrantModel> list;
    private Context context;
    Activity myActivity;
    Global global;

    public MyLayoutListRecords(List<RegistrantModel> list, Context context, Activity activity) {
        this.list = list;
        this.context = context;
        myActivity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return list.get(pos).getlEventID();//list.get(pos).getId();
        //just return 0 if your list items do not have an Id variable.
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.my_layout_list_records, null);
        }

        //Handle TextView and display string from your list
        TextView listItemText = (TextView)view.findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.list_item_string1);
        listItemText.setText(list.get(position).getsFirstName());

        TextView listItemText2 = (TextView)view.findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.list_item_string2);
        listItemText2.setText(list.get(position).getsLastName());

        TextView listItemText3 = (TextView)view.findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.list_item_string3);
        listItemText3.setText(list.get(position).getsCompany());

        return view;
    }
}
