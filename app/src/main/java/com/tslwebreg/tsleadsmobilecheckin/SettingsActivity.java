package com.tslwebreg.tsleadsmobilecheckin;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class SettingsActivity extends AppCompatActivity {
    Global global;
    Context context = this;
    SharedPreferences sharedPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        global=((Global)getApplicationContext());

        sharedPrefs = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);

        EditText txtPrinterIP =(EditText)findViewById(R.id.txtPrinterIP);
        txtPrinterIP.setText(global.printerIP);

        EditText txtPrinterPort =(EditText)findViewById(R.id.txtPrinterPort);
        txtPrinterPort.setText(global.printerPort);

        EditText txtPrinterMAC =(EditText)findViewById(R.id.txtPrinterMAC);
        txtPrinterMAC.setText(global.printerMAC);

        EditText txtTimerSearch =(EditText)findViewById(R.id.txtTimerSearch);
        txtTimerSearch.setText(global.timerSearch);

        EditText txtTimerPrint =(EditText)findViewById(R.id.txtTimerPrint);
        txtTimerPrint.setText(global.timerPrint);

        SwitchCompat auto_print_switch =(SwitchCompat)findViewById(R.id.auto_print_switch);
        auto_print_switch.setChecked(global.isAutoPrint);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.miSave) {
            SharedPreferences.Editor editor = sharedPrefs.edit();

            EditText txtPrinterIP =(EditText)findViewById(R.id.txtPrinterIP);
            EditText txtPrinterPort =(EditText)findViewById(R.id.txtPrinterPort);
            EditText txtPrinterMAC =(EditText)findViewById(R.id.txtPrinterMAC);
            EditText txtTimerSearch =(EditText)findViewById(R.id.txtTimerSearch);
            EditText txtTimerPrint =(EditText)findViewById(R.id.txtTimerPrint);
            SwitchCompat auto_print_switch =(SwitchCompat)findViewById(R.id.auto_print_switch);

            editor.putString("printerIP", txtPrinterIP.getText().toString());
            editor.putString("printerPort", txtPrinterPort.getText().toString());
            editor.putString("printerMAC", txtPrinterMAC.getText().toString());
            editor.putString("timerSearch", txtTimerSearch.getText().toString());
            editor.putString("timerPrint", txtTimerPrint.getText().toString());
            editor.putBoolean("isAutoPrint", auto_print_switch.isChecked());
            editor.commit();

            global.printerIP = txtPrinterIP.getText().toString();
            global.printerPort = txtPrinterPort.getText().toString();
            global.printerMAC = txtPrinterMAC.getText().toString();
            global.timerSearch = txtTimerSearch.getText().toString();
            global.timerPrint = txtTimerPrint.getText().toString();
            global.isAutoPrint = auto_print_switch.isChecked();

            new AlertDialog.Builder(context)
                    .setTitle("Save")
                    .setMessage("The settings have been saved.")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
