package com.tslwebreg.tsleadsmobilecheckin;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.honeywell.mobility.print.LabelPrinter;
import com.honeywell.mobility.print.LabelPrinterException;
import com.honeywell.mobility.print.PrintProgressEvent;
import com.honeywell.mobility.print.PrintProgressListener;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.comm.TcpConnection;
import com.zebra.sdk.printer.PrinterStatus;
import com.zebra.sdk.printer.SGD;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;
import com.zebra.sdk.printer.ZebraPrinterLinkOs;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Step2Activity extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;
    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    static ListView lv;
    static RegistrantDatabase registrantDatabase;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    final Context context = this;
    private final Handler mHideHandler = new Handler();
    //private com.tslwebreg.tsleadsmobilecheckin.UIHelper helper = new com.tslwebreg.tsleadsmobilecheckin.UIHelper(this);
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };
    Global global;
    int totalSize = 0;
    int downloadedSize = 0;
    String selectedFirstName, selectedlastname;
    String lCurID = "";
    //runs without a timer by reposting this handler at the end of the runnable
    long startTime;
    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            long nowTime = System.currentTimeMillis();
            if (nowTime - startTime > (Integer.parseInt(global.timerSearch) * 1000)) {
                timerHandler.removeCallbacks(timerRunnable);
                finish();
            }
            timerHandler.postDelayed(this, 1000);
        }
    };
    long startTime2;
    Handler timerHandler2 = new Handler();
    Runnable timerRunnable2 = new Runnable() {
        @Override
        public void run() {
            long nowTime = System.currentTimeMillis();
            if (nowTime - startTime2 > (Integer.parseInt((global.timerPrint)) * 1000)) {
                timerHandler2.removeCallbacks(timerRunnable2);
                finish();
            }
            timerHandler2.postDelayed(this, 1000);
        }
    };
    long startTime3;
    Handler timerHandler3 = new Handler();
    ProgressDialog progress;
    private String lastName = "", companyName = "";
    private Connection connection = null;
    private String jsonCmdAttribStr = null;
    private View mContentView;
    private int lguestId = 0;
    private boolean selected = false;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    private List<RegistrantModel> list = new ArrayList<>();
    Runnable timerRunnable3 = new Runnable() {
        @Override
        public void run() {

            timerHandler3.removeCallbacks(timerRunnable3);
            lv.requestFocusFromTouch();
            lv.setItemChecked(0, true);
            lv.setSelection(0);
            String lCurID = String.valueOf(list.get(0).getlRegID());
            String result = global.getRegistrantInfo(global.eventsList.get(global._event_current_index).get_eventID(), lCurID, lguestId);
            if (result == "") {
                String result2 = printBadge(global.eventsList.get(global._event_current_index).get_eventID(), global.eventsList.get(global._event_current_index).get_eventAccountID());
            }
        }
    };
    private Handler regHandler = new Handler(new Handler.Callback() {

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public boolean handleMessage(Message message) {

            Log.e("@@@@@@@@@@@", "INSIDE COMPOSE CHECK HANDLER");

            Bundle bundle = message.getData();

            String string = bundle.getString("status");

            if (Objects.equals(string, "done")) {
                setAdapter();

            } else {

                progress.dismiss();
                new AlertDialog.Builder(Step2Activity.this)
                        .setTitle("Next Step")
                        .setMessage("No records were found.")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
            return false;
        }
    });
    private final Runnable mMessageSender = new Runnable() {

        public void run() {

            try {

                Thread.sleep(1000);

                List<RegistrantModel> finalRegList = new ArrayList<>();

                if (!lastName.equals("")) {

                    finalRegList.clear();
                    finalRegList = registrantDatabase.getRegistrantDao().getRegistrantsListOntheBasisofLastName(global.eventsList.get(global._event_current_index).get_eventID(), lastName);

                } else if (!companyName.equals("")) {

                    finalRegList.clear();
                    finalRegList = registrantDatabase.getRegistrantDao().getRegistrantsListOntheBasisofCompanyName(global.eventsList.get(global._event_current_index).get_eventID(), companyName);
                } else if (!global.lCurID.equals("") && global.isScan) {

                    finalRegList = registrantDatabase.getRegistrantDao().fetchRegistrantByEventAndUniqueID(global.eventsList.get(global._event_current_index).get_eventID(), global.lCurID, "0");

                }

                if (finalRegList != null && finalRegList.size() > 0) {

                    for (int i = 0; i < finalRegList.size(); i++) {

                        if (finalRegList.get(i).getlRegType().equals("-1") && finalRegList.get(i).getlGuestID().equals("0")) {
                        } else if (!finalRegList.get(i).getnStatus().equals("0")) {

                        } else {
                            list.add(finalRegList.get(i));
                        }
                    }
                }

                if (list != null && list.size() > 0) {
                    Message msg = regHandler.obtainMessage();
                    Bundle bundle = new Bundle();
                    bundle.putString("status", "done");

                    msg.setData(bundle);
                    regHandler.sendMessage(msg);


                } else {

                    Message msg = regHandler.obtainMessage();
                    Bundle bundle = new Bundle();
                    bundle.putString("status", "");

                    msg.setData(bundle);
                    regHandler.sendMessage(msg);
                }

            } catch (Exception e) {
                e.printStackTrace();
                progress.dismiss();
            }
        }
    };

    public static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step2);

        progress = new ProgressDialog(Step2Activity.this);
        progress.setMessage("Getting Records");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.setProgress(0);
        progress.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        progress.show();

        registrantDatabase = RegistrantDatabase.getInstance(this);

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("lastname")) {
                lastName = getIntent().getExtras().get("lastname").toString();
            } else if (getIntent().getExtras().containsKey("companyname")) {
                companyName = getIntent().getExtras().get("companyname").toString();
            }
        }

        lv = (ListView) findViewById(R.id.lstRecords);
        if (registrantDatabase != null) {
            new Thread(mMessageSender).start();
        } else {
            registrantDatabase = RegistrantDatabase.getInstance(this);
            new Thread(mMessageSender).start();
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
        }

        startTime = System.currentTimeMillis();
        timerHandler.postDelayed(timerRunnable, 1000);

        verifyStoragePermissions(Step2Activity.this);

        global = ((Global) getApplicationContext());
        if (global.isScan) {
            lCurID = global.lCurID;
        }
        mVisible = true;
        mContentView = findViewById(R.id.step2_content);

        TextView txtTop = (TextView) findViewById(R.id.lblTop2);
        txtTop.setText(global.eventsList.get(global._event_current_index).get_eventHeader3());
        TextView txtMiddle = (TextView) findViewById(R.id.lblMiddle2);
        txtMiddle.setText(global.eventsList.get(global._event_current_index).get_eventHeader4());

        Button btnPrint = (Button) findViewById(R.id.btnPrint2);
        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (lCurID.equals("") || lguestId == 0) {

                    new AlertDialog.Builder(Step2Activity.this)
                            .setTitle("Alert!!")
                            .setMessage("Please Click on Your Record Before Printing!!")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
//                                    finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                } else
                    printFile();

            }
        });

        Button btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selected) {
                    selected = false;
                    Intent intent = new Intent(context, ListProgramsActivity.class);
                    intent.putExtra("regId", lCurID);
                    intent.putExtra("firstname", selectedFirstName);
                    intent.putExtra("lastname", selectedlastname);
                    context.startActivity(intent);
                    finish();
                } else {

                    new AlertDialog.Builder(Step2Activity.this)
                            .setTitle("Alert!!")
                            .setMessage("Please select any Record!!")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
//                                    finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }
        });
        SharedPreferences sharedPrefs = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);

        if (sharedPrefs.getBoolean("isAutoPrint", false)) {
            btnPrint.setVisibility(View.GONE);
            btnNext.setVisibility(View.VISIBLE);
        } else {
            btnNext.setVisibility(View.GONE);
            btnPrint.setVisibility(View.VISIBLE);
        }

        Button btnFinish = (Button) findViewById(R.id.btnFinish);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


//        final ArrayList<Record> recordArrayList = new ArrayList<>();
//        Gson gson = new Gson();
//        String json = sharedPrefs.getString("registrant_list", "");
//        Type type = new TypeToken<List<RegistrantModel>>() {}.getType();
//        final List<RegistrantModel> registrantModels = gson.fromJson(json, type);


//        final ProgressDialog progress = new ProgressDialog(Step2Activity.this);
//        progress.setMessage("Getting Records");
//        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progress.setIndeterminate(true);
//        progress.setCancelable(false);
//        progress.setProgress(0);
//        progress.getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );
//        progress.show();

//        new Handler().post(new Runnable() {
//            @Override
//            public void run() {
//                for(int i = 0; i < global.recordsList.size() ; i++){
//
//                    Log.e("@Record Registrants DB", "----Id  " + global.recordsList.get(i).get_ID() + "---");
//
//                    for(int j = 0 ; j < registrantModels.size(); j++){
//
//                        Log.e("@@@Registerant sahil", "----Id  " +Integer.parseInt(registrantModels.get(j).getlRegID()) + "---");
//
//                        if(global.recordsList.get(i).get_ID() == Integer.parseInt(registrantModels.get(j).getlRegID())){
//
//                            if(registrantModels.get(j).getlRegType().equals("-1") && registrantModels.get(j).getlGuestID().equals("0")){
////                                continue;
//                            }else{
//                                recordArrayList.add(global.recordsList.get(i));
//                            }
//                        }
//                    }
//                }
//                progress.cancel();
//                MyLayoutListRecords adapter = new MyLayoutListRecords(recordArrayList, context, Step2Activity.this);
//                lv.setAdapter(adapter);
//            }
//        });
//
//        MyLayoutListRecords adapter = new MyLayoutListRecords(global.recordsList, context, Step2Activity.this);
//        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
                startTime = System.currentTimeMillis();
                lCurID = list.get(pos).getlRegID();
                lguestId = list.get(pos).getUniqueId();
                selectedFirstName = list.get(pos).getsFirstName();
                selectedlastname = list.get(pos).getsLastName();
                selected = true;
            }
        });
        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                startTime = System.currentTimeMillis();
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
                startTime = System.currentTimeMillis();
            }
        });

        if (list.size() == 1) {
            //removed auto print on Oct 23 2018
            //timerHandler3.postDelayed(timerRunnable3, 2000);
        }

        global.isAutoPrint = sharedPrefs.getBoolean("isAutoPrint", false);
        if (global.isAutoPrint && global.isScan) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    printFile();

                }
            }, 2000);
        }
    }

    private void setAdapter() {

        MyLayoutListRecords adapter = new MyLayoutListRecords(list, this, Step2Activity.this);
        lv.setAdapter(adapter);
        progress.dismiss();
    }

    private void printFile() {

        String result = global.getRegistrantInfo(global.eventsList.get(global._event_current_index).get_eventID(), lCurID, lguestId);
        Log.e("@@ result", "------------------------" + result);

        if (result.equals("")) {

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lguestId = 0;
            lCurID = "";
            String result2 = printBadge(global.eventsList.get(global._event_current_index).get_eventID(), global.eventsList.get(global._event_current_index).get_eventAccountID());
            startTime2 = System.currentTimeMillis();
            timerHandler2.postDelayed(timerRunnable2, 1000);
        } else {
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
//        global.isScan = false;
    }

    private void readAssetFiles() {
        InputStream input = null;
        ByteArrayOutputStream output = null;
        AssetManager assetManager = getAssets();
        Toast toast;

        try {
            input = assetManager.open("printer_profiles.JSON");
            output = new ByteArrayOutputStream(8000);

            byte[] buf = new byte[1024];
            int len;
            while ((len = input.read(buf)) > 0) {
                output.write(buf, 0, len);
            }
            input.close();
            input = null;

            output.flush();
            output.close();
            jsonCmdAttribStr = output.toString();
            output = null;
        } catch (Exception ex) {
            toast = Toast.makeText(this, "Error reading asset file: printer_profiles.JSON", Toast.LENGTH_SHORT);
            toast.show();
        } finally {
            try {
                if (input != null) {
                    input.close();
                    input = null;
                }

                if (output != null) {
                    output.close();
                    output = null;
                }
            } catch (IOException e) {
            }
        }
    }

    private String printBadge(int eventID, int eventAccountID) {
        //Zebra
        try {
            //get ZPL code to send to printer
            /*URL url = new URL("http://www.tslwebreg.com/tslcheckin/getZPLCode.php?event_id=" + eventID + "&user_id=" + eventAccountID + "&reg_id=" + regID);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.connect();

            File SDCardRoot = Environment.getExternalStorageDirectory();
            global.zplFileName = "temp.LBL";

            InputStream inputStream = urlConnection.getInputStream();
            totalSize = urlConnection.getContentLength();
            byte[] buffer = new byte[1024];
            FileOutputStream os = this.openFileOutput(global.zplFileName, Context.MODE_PRIVATE);
            int bufferLength = 0;
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                os.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                runOnUiThread(new Runnable() {
                    public void run() {
                        float per = ((float) downloadedSize / totalSize) * 100;
                    }
                });
            }
            os.flush();
            os.close();

            //zebra
            new Thread(new Runnable() {
                public void run() {
                    Looper.prepare();
                    connectAndPrint();
                    Looper.loop();
                    Looper.myLooper().quit();
                }
            }).start();*/


            //INtermec
            InputStream inputStream;

            //save to file
            int intermecFileExists = 0;
            File myDirectory = new File(Environment.getExternalStorageDirectory(), "CheckIn");
            if (!myDirectory.exists()) {
                myDirectory.mkdirs();
            }

            File fileCheck = new File(Environment.getExternalStorageDirectory() + File.separator + "CheckIn", "intermec_" + global.eventsList.get(global._event_current_index).get_eventID() + ".txt");
            if (fileCheck.exists()) {
                intermecFileExists = 1;
            } else {
                //get code to send to printer
                URL url = new URL("http://www.tslwebreg.com/tslcheckin/getIntermecCode.php?event_id=" + eventID + "&user_id=" + eventAccountID);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);
                urlConnection.connect();

                inputStream = urlConnection.getInputStream();
                try {
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "CheckIn", "intermec_" + global.eventsList.get(global._event_current_index).get_eventID() + ".txt");
                    OutputStream output = new FileOutputStream(file);
                    try {
                        byte[] buffer = new byte[4 * 1024]; // or other buffer size
                        int read;

                        while ((read = inputStream.read(buffer)) != -1) {
                            output.write(buffer, 0, read);
                        }

                        output.flush();
                    } finally {
                        output.close();
                    }
                } finally {
                    inputStream.close();
                }
            }

            fileCheck = new File(Environment.getExternalStorageDirectory() + File.separator + "CheckIn", "intermec_" + global.eventsList.get(global._event_current_index).get_eventID() + ".txt");
            inputStream = new FileInputStream(fileCheck);
            jsonCmdAttribStr = convertInputStreamToString(inputStream);

            //readAssetFiles();
            PrintTask task = new PrintTask();
            task.execute("PB22_Fingerprint", global.printerMAC);

            return "success";

        } catch (final MalformedURLException e) {
            //showError("Error : MalformedURLException " + e);
            e.printStackTrace();
            return e.toString();
        } catch (final IOException e) {
            //showError("Error : IOException " + e);
            e.printStackTrace();
            return e.toString();
        } catch (final Exception e) {
            //showError("Error : Please check your internet connection " + e);
            return e.toString();
        }

    }

    /**
     * Check for the printer status and language and send the test file to the printer,implements best practices to show status of the printer.
     */
    private void connectAndPrint() {

        try {
            connection = new TcpConnection(global.printerIP, Integer.valueOf(global.printerPort));
        } catch (NumberFormatException e) {
            displayFloatingMessage("Unable to print badge.\nPort number is invalid");
            return;
        }

        try {
            connection.open();
            ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);
            ZebraPrinterLinkOs linkOsPrinter = ZebraPrinterFactory.createLinkOsPrinter(printer);
            PrinterStatus printerStatus = (linkOsPrinter != null) ? linkOsPrinter.getCurrentStatus() : printer.getCurrentStatus();
            getPrinterStatus();
            String message1 = "";
            if (printerStatus.isReadyToPrint) {
                displayFloatingMessage(sendFileToPrinter(printer));
            } else if (printerStatus.isHeadOpen) {
                message1 = "Your badge was not printed. \nHead Open \nPlease Close Printer Head to Print.";
            } else if (printerStatus.isPaused) {
                message1 = "Your badge was not printed. \nThe printer is paused.";
            } else if (printerStatus.isPaperOut) {
                message1 = "Your badge was not printed. \nMedia Out \nPlease Load Media to Print.";
            }
            if (message1 != "") {
                new AlertDialog.Builder(Step2Activity.this)
                        .setTitle("Print")
                        .setMessage(message1)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }

            connection.close();
        } catch (ConnectionException e) {
            displayFloatingMessage("Unable to print badge.\n" + e.getMessage());
        } catch (ZebraPrinterLanguageUnknownException e) {
            displayFloatingMessage("Unable to print badge.\n" + e.getMessage());
        } finally {
            //helper.dismissLoadingDialog();
        }
    }

    private void displayFloatingMessage(String textToDisplay) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast_layout,
                (ViewGroup) findViewById(R.id.toast_layout_root));
        // get the reference of TextView and ImageVIew from inflated layout
        TextView toastTextView = (TextView) layout.findViewById(R.id.toastTextView);
        ImageView toastImageView = (ImageView) layout.findViewById(R.id.toastImageView);
        toastTextView.setText(textToDisplay);
        toastImageView.setImageResource(R.drawable.print);
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setView(layout);
        toast.show();
    }

    /**
     * This method implements best practices to check the language of the printer and set the language of the printer to ZPL.
     *
     * @return printer
     * @throws ConnectionException
     */
    private void getPrinterStatus() throws ConnectionException {
        String printerLanguage = SGD.GET("device.languages", connection);
        final String displayPrinterLanguage = "Printer Language is " + printerLanguage;
        SGD.SET("device.languages", "hybrid_xml_zpl", connection);

        Step2Activity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(Step2Activity.this, displayPrinterLanguage + "\n" + "Language set to ZPL", Toast.LENGTH_LONG).show();
            }
        });
    }

    private String sendFileToPrinter(ZebraPrinter printer) {
        try {
            File filepath = getFileStreamPath(global.zplFileName);
            printer.sendFileContents(filepath.getAbsolutePath());
            return "Your badge is printing.";
        } catch (ConnectionException e1) {
            return "Error sending file to printer";
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        hide();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN
        );
    }

    @Override
    public void onUserInteraction() {
        Intent intent = new Intent(context, Step2Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        intent.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    public class PrintTask extends AsyncTask<String, Integer, String> {
        private static final String PROGRESS_CANCEL_MSG = "Printing cancelled\n";
        private static final String PROGRESS_COMPLETE_MSG = "Printing completed\n";
        private static final String PROGRESS_ENDDOC_MSG = "End of label printing\n";
        private static final String PROGRESS_FINISHED_MSG = "Printer connection closed\n";
        private static final String PROGRESS_NONE_MSG = "Unknown progress message\n";
        private static final String PROGRESS_STARTDOC_MSG = "Start printing label\n";

        /**
         * Runs on the UI thread before doInBackground(Params...).
         */
        @Override
        protected void onPreExecute() {

        }

        /**
         * This method runs on a background thread. The specified parameters
         * are the parameters passed to the execute method by the caller of
         * this task. This method can call publishProgress to publish updates
         * on the UI thread.
         */
        @Override
        protected String doInBackground(String... args) {
            LabelPrinter lp = null;
            String sResult = null;
            String sPrinterID = args[0];
            String sPrinterURI = "bt://" + args[1];
            final Toast[] toast = new Toast[1];

            LabelPrinter.ExtraSettings exSettings = new LabelPrinter.ExtraSettings();
            exSettings.setContext(Step2Activity.this);

            try {
                lp = new LabelPrinter(
                        jsonCmdAttribStr,
                        sPrinterID,
                        sPrinterURI,
                        exSettings);

                // Registers to listen for the print progress events.
                lp.addPrintProgressListener(new PrintProgressListener() {
                    @Override
                    public void receivedStatus(PrintProgressEvent aEvent) {
                        // Publishes updates on the UI thread.
                        publishProgress(aEvent.getMessageType());
                    }
                });

                // A retry sequence in case the bluetooth socket is temporarily not ready
                int numtries = 0;
                int maxretry = 2;
                while (numtries < maxretry) {
                    try {
                        lp.connect();  // Connects to the printer
                        break;
                    } catch (LabelPrinterException ex) {
                        numtries++;
                        Thread.sleep(1000);
                    }
                }
                if (numtries == maxretry) lp.connect();//Final retry

                // Sets up the variable dictionary.
                LabelPrinter.VarDictionary varDataDict = new LabelPrinter.VarDictionary();
                varDataDict.put("FullName", global.regFirstNameTemp + " " + global.regLastNameTemp);
                varDataDict.put("FirstName", global.regFirstNameTemp);
                varDataDict.put("MiddleName", global.regMiddleNameTemp);
                varDataDict.put("LastName", global.regLastNameTemp);
                varDataDict.put("Suffix", global.regSuffixTemp);
                varDataDict.put("Credentials", global.regCredentialsTemp);
                varDataDict.put("Title", global.regTitleTemp);
                varDataDict.put("CompanyName", global.regCompanyTemp);
                varDataDict.put("Address1", global.regAddress1Temp);
                varDataDict.put("Address2", global.regAddress2Temp);
                varDataDict.put("City", global.regCityTemp);
                varDataDict.put("State", global.regStateTemp);
                varDataDict.put("Zip", global.regZipTemp);
                varDataDict.put("Country", global.regCountryTemp);
                varDataDict.put("Phone", global.regPhoneTemp);
                varDataDict.put("Cell", global.regCellTemp);
                varDataDict.put("Fax", global.regFaxTemp);
                varDataDict.put("Email", global.regEmailTemp);
                if (global.regStateTemp != "") {
                    varDataDict.put("CityState", global.regCityTemp + ", " + global.regStateTemp);
                } else {
                    varDataDict.put("CityState", global.regCityTemp);
                }
                varDataDict.put("RegType", global.regOther1Temp);
//                varDataDict.put("RegID", global.regLeadIDTemp);
                varDataDict.put("RegID", String.valueOf(global.uniqueID));

                // Prints the ItemLabel as defined in the printer_profiles.JSON file.
                lp.writeLabel("ItemLabel", varDataDict);

                sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
            } catch (final LabelPrinterException ex) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String message = "LabelPrinterException: " + ex.getMessage();
                        toast[0] = Toast.makeText(context, message, Toast.LENGTH_LONG);
                        toast[0].show();
                    }
                });

            } catch (final Exception ex) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String message = "Unexpected exception." + ex.getMessage();
                        toast[0] = Toast.makeText(context, message, Toast.LENGTH_LONG);
                        toast[0].show();
                    }
                });
            } finally {
                if (lp != null) {
                    try {
                        // Notes: To ensure the data is transmitted to the printer
                        // before the connection is closed, both PB22_Fingerprint and
                        // PB32_Fingerprint printer entries specify a PreCloseDelay setting
                        // in the printer_profiles.JSON file included with this sample.
                        lp.disconnect();  // Disconnects from the printer
                        lp.close();  // Releases resources
                    } catch (Exception ex) {
                    }
                }
            }

            // The result string will be passed to the onPostExecute method
            // for display in the the Progress and Status text box.
            return sResult;
        }

        /**
         * Runs on the UI thread after publishProgress is invoked. The
         * specified values are the values passed to publishProgress.
         */
        @Override
        protected void onProgressUpdate(Integer... values) {
            // Access the values array.
            int progress = values[0];
            Toast toast;
            String message = "";
            switch (progress) {
                case PrintProgressEvent.MessageTypes.CANCEL:
                    message = PROGRESS_CANCEL_MSG;
                    break;
                case PrintProgressEvent.MessageTypes.COMPLETE:
                    //message = PROGRESS_COMPLETE_MSG;
                    break;
                case PrintProgressEvent.MessageTypes.ENDDOC:
                    //message = PROGRESS_ENDDOC_MSG;
                    break;
                case PrintProgressEvent.MessageTypes.FINISHED:
                    //message = PROGRESS_FINISHED_MSG;
                    startTime2 = System.currentTimeMillis();
                    timerHandler2.postDelayed(timerRunnable2, 1000);
                    break;
                case PrintProgressEvent.MessageTypes.STARTDOC:
                    //message = PROGRESS_STARTDOC_MSG;
                    break;
                default:
                    //message = PROGRESS_NONE_MSG;
                    break;
            }

            if (message != "") {
                toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
                toast.show();
            }
        }

        /**
         * Runs on the UI thread after doInBackground method. The specified
         * result parameter is the value returned by doInBackground.
         */
        @Override
        protected void onPostExecute(String result) {
            // Displays the result (number of bytes sent to the printer or
            // exception message) in the Progress and Status text box.
            if (result != null) {
                // textMsg.append(result);
            }

            // Enables the Print button.
            //buttonPrint.setEnabled(true);
        }
    } //endofclass PrintTask

}
