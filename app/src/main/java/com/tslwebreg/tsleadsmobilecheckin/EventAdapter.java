package com.tslwebreg.tsleadsmobilecheckin;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> implements RadioAdapter.radiochildclicklistener, CheckBoxAdapter.checkboxselectlistener {

    private Context context;
    private HashMap<String, ListProgramsActivity.RecordEvent> listMap;
    private String[] mkeys;
    private ArrayList<ListProgramsActivity.RecordEvent> recordEventList;
    private answercheckedlistener answercheckedlistener;

    public EventAdapter(Context context, HashMap<String, ListProgramsActivity.RecordEvent> listMap, ArrayList<ListProgramsActivity.RecordEvent> recordEventList, answercheckedlistener answercheckedlistener) {
        this.context = context;
        this.listMap = listMap;
        mkeys = listMap.keySet().toArray(new String[listMap.size()]);
        this.recordEventList = recordEventList;
        this.answercheckedlistener = answercheckedlistener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.parent_item_event, viewGroup, false);
        return new EventAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

        viewHolder.dateText.setText(recordEventList.get(i).getsName());
        if (recordEventList.get(i).getnType().equals("3")) {
            RadioAdapter radioAdapter = new RadioAdapter(context, recordEventList.get(i), recordEventList.get(i).getModelAnswerList(), this);
            viewHolder.eventDataRecyclerView.setAdapter(radioAdapter);

        } else if (recordEventList.get(i).getnType().equals("2")) {
            viewHolder.aEditText.setVisibility(View.GONE);
            viewHolder.aSpinner.setVisibility(View.GONE);
            viewHolder.eventDataRecyclerView.setVisibility(View.VISIBLE);
            CheckBoxAdapter checkBoxAdapter = new CheckBoxAdapter(context, recordEventList.get(i), recordEventList.get(i).getModelAnswerList(), this);
            viewHolder.eventDataRecyclerView.setAdapter(checkBoxAdapter);
        } else if (recordEventList.get(i).getnType().equals("0")) {
            // Show Edit text
            viewHolder.aEditText.setVisibility(View.VISIBLE);
            viewHolder.aSpinner.setVisibility(View.GONE);
            viewHolder.eventDataRecyclerView.setVisibility(View.GONE);
            viewHolder.aEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override
                public void afterTextChanged(Editable editable) {
                    answercheckedlistener.onclick(recordEventList.get(i), viewHolder.aEditText.getText().toString());
                }
            });
        } else if (recordEventList.get(i).getnType().equals("1")) {
            // Show spinner
            viewHolder.aEditText.setVisibility(View.GONE);
            viewHolder.eventDataRecyclerView.setVisibility(View.GONE);
            viewHolder.aSpinner.setVisibility(View.VISIBLE);
            final List<String> list1 = new ArrayList<>();

            for (int j = 0; j < recordEventList.get(i).getModelAnswerList().size(); j++) {
                list1.add(recordEventList.get(i).getModelAnswerList().get(j).getsAnswer());
            }

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                    (context, android.R.layout.simple_spinner_item, list1
                    ); //selected item will look like a spinner set from XML
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                    .simple_spinner_dropdown_item);
            viewHolder.aSpinner.setAdapter(spinnerArrayAdapter);
            viewHolder.aSpinner.setSelection(list1.indexOf(recordEventList.get(i).getsAnswer()));

            viewHolder.aSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                    String answer=list1.get(pos);
                    ListProgramsActivity.RecordEvent recordEvent=recordEventList.get(i);

                    for(ModelAnswer modelAnswer:recordEvent.getModelAnswerList()){
                        if(modelAnswer.getsAnswer().equals(answer))
                            recordEvent.setCheckedAnswerId(modelAnswer.getlAnswerID());
                    }
                    answercheckedlistener.onclick(recordEvent,answer);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return recordEventList.size();
    }

    @Override
    public void onRadioClick(ListProgramsActivity.RecordEvent event, String answer) {
        answercheckedlistener.onclick(event, answer);
        event.setsAnswer(answer);
        notifyDataSetChanged();
    }

    @Override
    public void onCheckboxClick(ListProgramsActivity.RecordEvent event, String answer) {
        answercheckedlistener.onclick(event, answer);
        event.setsAnswer(answer);
        notifyDataSetChanged();
    }

    public interface answercheckedlistener {
        void onclick(ListProgramsActivity.RecordEvent event, String answer);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView dateText;
        private RecyclerView eventDataRecyclerView;
        private EditText aEditText;
        private Spinner aSpinner;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            aEditText = (EditText) itemView.findViewById(R.id.answerEditText);
            aSpinner  = (Spinner)  itemView.findViewById(R.id.answerDropDown);
            dateText  = (TextView) itemView.findViewById(R.id.date_text);
            eventDataRecyclerView = (RecyclerView) itemView.findViewById(R.id.item_recyclerView);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
            eventDataRecyclerView.setItemAnimator(new DefaultItemAnimator());
            eventDataRecyclerView.setLayoutManager(layoutManager);
        }
    }
}