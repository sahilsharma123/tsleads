package com.tslwebreg.tsleadsmobilecheckin;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.List;

public class RadioAdapter extends RecyclerView.Adapter<RadioAdapter.ViewHolder> {

    Context context;
    ListProgramsActivity.RecordEvent recordEvent;
    radiochildclicklistener chilclicklistener;
    List<ModelAnswer> modelAnswerList;

    public RadioAdapter(Context context, ListProgramsActivity.RecordEvent recordEvent, List<ModelAnswer> modelAnswerList, radiochildclicklistener chilclicklistener) {
        this.context = context;
        this.recordEvent = recordEvent;
        this.chilclicklistener = chilclicklistener;
        this.modelAnswerList = modelAnswerList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.radio_item, viewGroup, false);
        return new RadioAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        for (final ModelAnswer answer:modelAnswerList ) {
            final RadioButton radioButton = new RadioButton(context);
            radioButton.setText(answer.getsAnswer());

            if(recordEvent.getCheckedAnswerId().equals(answer.getlAnswerID())){

                radioButton.setChecked(true);
            }

            viewHolder.radioGroup.addView(radioButton);
            viewHolder.radioGroup.invalidate();

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    boolean checked = ((RadioButton) view).isChecked();

                    if (checked) {

                        String answerStr = ((RadioButton) view).getText().toString();
                        recordEvent.setsAnswer(answerStr);
                        recordEvent.setChecked(true);
                        recordEvent.setCheckedAnswerId(answer.getlAnswerID());
                        chilclicklistener.onRadioClick(recordEvent, answerStr);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public interface radiochildclicklistener {
        void onRadioClick(ListProgramsActivity.RecordEvent event, String answer);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private RadioGroup radioGroup;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            radioGroup = (RadioGroup) itemView.findViewById(R.id.radioCheckAnswer);
        }
    }
}