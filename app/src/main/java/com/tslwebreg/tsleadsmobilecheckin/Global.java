package com.tslwebreg.tsleadsmobilecheckin;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by fdemers on 12/23/2016.
 */
public class Global extends Application {
    private static RegistrantDatabase registrantDatabase;
    final ArrayList<Event> eventsList = new ArrayList<Event>();
    final Context context = this;
    public ArrayList<Record> recordsList = new ArrayList<Record>();
    String devName;
    String devSerial;
    int _event_current_index = 0;
    SQLiteDatabase mainDB;
    Context mainContext;
    int bAutoSyncRunning = 0;
    int bAddingNewEvent = 0;
    int regScanIDTemp = 0;
    String regLeadIDTemp = "";
    String regPrefixTemp = "";
    String regFirstNameTemp = "";
    String regMiddleNameTemp = "";
    String regLastNameTemp = "";
    String regSuffixTemp = "";
    String regCredentialsTemp = "";
    String regTitleTemp = "";
    String regCompanyTemp = "";
    String regAddress1Temp = "";
    String regAddress2Temp = "";
    String regAddress3Temp = "";
    String regCityTemp = "";
    String regStateTemp = "";
    String regZipTemp = "";
    String regCountryTemp = "";
    String regPhoneTemp = "";
    String regCellTemp = "";
    String regFaxTemp = "";
    String regEmailTemp = "";
    String regUpdatedOnTemp = "";
    String regOther1Temp = "";
    String regOther2Temp = "";
    String regOther3Temp = "";
    String regOther4Temp = "";
    String regOther5Temp = "";
    String regOther6Temp = "";
    String regOther7Temp = "";
    String regOther8Temp = "";
    String regOther9Temp = "";
    String regOther10Temp = "";
    int regPrinted = 1;
    int uniqueID = 0;
    String printerIP = "";
    String printerPort = "";
    String printerMAC = "";
    String zplFileName = "";
    String timerSearch = "60";
    String timerPrint = "10";
    boolean isAutoPrint = false;
    boolean isScan = false;
    String lCurID = "";
    int stcount = 0;

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    static String NullToString(String Value) {
        return Value == null ? "" : Value.toString();
    }

    private static void saveRegistrantData(Context context, final List<RegistrantModel> data) {

        try {
            registrantDatabase = RegistrantDatabase.getInstance(context);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    registrantDatabase.getRegistrantDao().insertMultipleCategories(data);
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);


        devSerial = Build.SERIAL;

        if (devSerial.equalsIgnoreCase("0123456789ABCDEF")) {
            try {
                List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface ntwInterface : interfaces) {
                    if (ntwInterface.getName().equalsIgnoreCase("p2p0")) {
                        byte[] byteMac = ntwInterface.getHardwareAddress();
                        if (byteMac != null) {
                            StringBuilder strBuilder = new StringBuilder();
                            for (int i = 0; i < byteMac.length; i++) {
                                strBuilder.append(String.format("%02X:", byteMac[i]));
                            }

                            if (strBuilder.length() > 0) {
                                strBuilder.deleteCharAt(strBuilder.length() - 1);
                            }

                            devSerial = strBuilder.toString();
                            devSerial = devSerial.replace(":", "");
                        }
                    }
                }
            } catch (Exception e) {

            }
        }

        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        if (myDevice != null) {
            devName = myDevice.getName();
        } else {
            devName = Build.USER;
        }

        //open database
        DataBaseHelper myDbHelper = new DataBaseHelper(this);

        try {
            myDbHelper.createDataBase();
            appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + "Database created");

        } catch (IOException ioe) {
            appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + "Unable to create database");
            throw new Error("Unable to create database");
        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + "Unable to open database");
            throw new Error("Unable to open database.");
        }

        mainDB = myDbHelper.getWritableDatabase();

        //CREATE INDEX index_name ON table_name;
        String mainSQl = "CREATE UNIQUE INDEX 'main'.'main1' ON 'Registrants' ('lEventID' ASC, 'lLeadID' ASC);";
        try {
            mainDB.execSQL(mainSQl);
        } catch (android.database.sqlite.SQLiteException ex) {

        }

        Cursor cursor = mainDB.rawQuery("SELECT * FROM Events ", null);
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                Event event = new Event();
                event.set_eventID(Integer.parseInt(cursor.getString(0)));
                event.set_eventName(cursor.getString(1));
                event.set_eventStart(cursor.getString(2));
                event.set_eventEnd(cursor.getString(3));
                event.set_eventLocation(cursor.getString(4));
                event.set_eventAccountID(Integer.parseInt(cursor.getString(5)));
                event.set_deviceSerialnumber(cursor.getString(6));
                event.set_eventLastSync(cursor.getString(7));

                event.set_eventHeader1(cursor.getString(8));
                event.set_eventHeader2(cursor.getString(9));
                event.set_eventHeader3(cursor.getString(10));
                event.set_eventHeader4(cursor.getString(11));
                event.set_eventPicture(cursor.getString(12));

                event.set_eventLastNameSearchOn(cursor.getInt(13));
                event.set_eventEmailSearchOn(cursor.getInt(14));
                event.set_eventScanOn(cursor.getInt(15));

                eventsList.add(event);
                cursor.moveToNext();
            }
        }
    }

    public void appendLog(String text) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateTime = sdf.format(new Date());
        String newText = currentDateTime + " " + text;

        File logFile = new File("/data/user/0/com.tslwebreg.tsleadsmobilecheckin/TSLCheckInlog.file");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(newText);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public boolean isNetworkConnected(Context ctx) {

        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL("https://www.google.com");
                urlConnection = (HttpURLConnection) url
                        .openConnection();
                urlConnection.setReadTimeout(2000);
                urlConnection.setConnectTimeout(2000);
                urlConnection.setRequestMethod("GET");

                int responseCode = urlConnection.getResponseCode();

                String response = "";
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        if (line != "") {
                            response += line;
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                //appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + e.toString());
                return false;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
        } else {
            return false;
        }
    }

    public String saveDeviceSerialNumber(int eventID, int lAccountID, String deviceSerialNumber) {
        URL url;
        HttpURLConnection urlConnection = null;
        try {

            url = new URL("http://www.tslwebreg.com/tslcheckin/saveDeviceSerialNumber.php");
            urlConnection = (HttpURLConnection) url
                    .openConnection();
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);

            OutputStream os = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));

            StringBuilder result = new StringBuilder();
            result.append(URLEncoder.encode("EventID", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(eventID), "UTF-8") + "&");
            result.append(URLEncoder.encode("AccountID", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(lAccountID), "UTF-8") + "&");
            result.append(URLEncoder.encode("DeviceSerialNumber", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(deviceSerialNumber), "UTF-8") + "&");
            result.append(URLEncoder.encode("DeviceName", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(devName), "UTF-8"));
            writer.write(result.toString());

            writer.flush();
            writer.close();
            os.close();
            int responseCode = urlConnection.getResponseCode();

            String response = "";
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    if (line != "") {
                        response += line;
                    }
                }
                return response;

            } else {
                return urlConnection.getResponseMessage();
            }

        } catch (Exception e) {
            appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + e.toString());
            return e.toString();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    public boolean updateLastSync(int reset2012, int eventID, String lastUpdatedOn) {
        String currentDateTime = "";

        if (reset2012 == 0) {
            if (lastUpdatedOn == "") {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                currentDateTime = sdf.format(new Date());
            } else {
                currentDateTime = lastUpdatedOn;//we will be here if there was an error or the internet connection was lost in UpdateScansOnline
            }
        } else {
            currentDateTime = "2012-12-30 12:00:00";
        }

        try {
            mainDB.execSQL("Update Events Set dtLastSync = '" + currentDateTime + "' Where lEventID = " + eventID);
        } catch (android.database.sqlite.SQLiteException ex) {
            appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
        }
        return true;
    }

    public String getAllRecordsFromOnline(Context ctx, int eventID, int eventAccountID, String deviceSerialNumber, int addingNewEvent, String lastSyncFromDB) {
        if (isNetworkConnected(ctx)) {
            String mainSQl = "";
            SQLiteStatement statement;
            String LeadIDTemp = "";
            String nStatus = "";
            String PrefixTemp = "";
            String FirstNameTemp = "";
            String MiddleNameTemp = "";
            String LastNameTemp = "";
            String SuffixTemp = "";
            String CredentialsTemp = "";
            String TitleTemp = "";
            String CompanyTemp = "";
            String Address1Temp = "";
            String Address2Temp = "";
            String Address3Temp = "";
            String CityTemp = "";
            String StateTemp = "";
            String ZipTemp = "";
            String CountryTemp = "";
            String PhoneTemp = "";
            String CellTemp = "";
            String FaxTemp = "";
            String EmailTemp = "";
            String UpdatedOnTemp = "";
            String Other1Temp = "";
            String Other2Temp = "";
            String Other3Temp = "";
            String Other4Temp = "";
            String Other5Temp = "";
            String Other6Temp = "";
            String Other7Temp = "";
            String Other8Temp = "";
            String Other9Temp = "";
            String Other10Temp = "";
            String lRegType = "";
            String lGuestId = "";
            String uniqueID = "";

            //we used the last dtUpdatedOn field from local Registrants table because we want to have the lastest records from the online Registrants table
            String LastSyncTemp = lastSyncFromDB;
            if (LastSyncTemp.equalsIgnoreCase("2012-12-30 12:00:00") == false) {
                mainSQl = "Select dtUpdatedOn from Registrants Where lEventID = " + eventID + " Order by dtUpdatedOn Desc LIMIT 1";
                Cursor cursor = mainDB.rawQuery(mainSQl, null);
                if (cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {
                        LastSyncTemp = cursor.getString(0);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
                if (LastSyncTemp.length() == 0) {
                    LastSyncTemp = "2012-12-30 12:00:00";
                }
            }

            //change spaces with ~
            LastSyncTemp = LastSyncTemp.replace(" ", "~");

            //put reg ids in array
            mainSQl = "Select UPPER(lLeadID) as lLeadID from Registrants Where lEventID = " + eventID;
            mainSQl += " Order by UPPER(lLeadID)";
            Cursor cursor = mainDB.rawQuery(mainSQl, null);
            ArrayList<String> listLeadIDs = new ArrayList();
            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    listLeadIDs.add(cursor.getString(0));
                    cursor.moveToNext();
                }
            }
            cursor.close();

            //put info from scans table into arrays
            mainSQl = "Select lScanID, lLeadID, dtUpdatedOn from Scans Where lEventID = " + eventID;
            mainSQl += " Order by lLeadID";
            cursor = mainDB.rawQuery(mainSQl, null);
            ArrayList<Integer> listScanIDTemp = new ArrayList();
            ArrayList<String> listScanLeadIDTemp = new ArrayList();
            ArrayList<String> listScanUpdatedOnTemp = new ArrayList();
            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    listScanIDTemp.add(cursor.getInt(0));
                    listScanLeadIDTemp.add(cursor.getString(1));
                    listScanUpdatedOnTemp.add(cursor.getString(2));
                    cursor.moveToNext();
                }
            }
            cursor.close();

            int bBatchDone = 0;
            int offset = 0;

            RegistrantModel registrantModel = new RegistrantModel();

            while (bBatchDone == 0) {
                URL url;
                String result = "";
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL("http://www.tslwebreg.com/getRegistrantsListForTSLCheckIN.php?user_id=" + eventAccountID + "&event_id=" + eventID + "&lastSync=" + LastSyncTemp + "&offset=" + offset);
                    Log.e("@@@@ Url", "----" + url);

                    urlConnection = (HttpURLConnection) url
                            .openConnection();
                    urlConnection.setReadTimeout(2000);
                    urlConnection.setConnectTimeout(2000);
                    urlConnection.setRequestMethod("GET");
                    InputStream in = urlConnection.getInputStream();
                    InputStreamReader isw = new InputStreamReader(in);
                    if (in != null) {
                        result = convertInputStreamToString(in);
                    } else {
                        return "failed";
                    }

                    registrantDatabase = RegistrantDatabase.getInstance(this);

//                     Log.e("@@@ res", "-----" + result);
                    JSONArray jsonArray = new JSONArray(result);
                    int arrayLength = jsonArray.length();

//                    Log.e("@@@ ARRAY LENGTH", "-----" + arrayLength);

                    ArrayList<Record> recordsList_temp = getAllRegistrants(eventID);

                    if (arrayLength > 0) {
                        mainDB.beginTransaction();
                        offset = offset + arrayLength;
                        final List<RegistrantModel> registrantModelList = new ArrayList<>();

                        for (int i = 0, count = arrayLength; i < count; i++) {
                            try {

                                registrantModel = new RegistrantModel();
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                //extract data
                                LeadIDTemp = jsonObject.getString("lRegID");
                                LeadIDTemp = LeadIDTemp.toUpperCase();
                                registrantModel.setlRegID(LeadIDTemp);
                                uniqueID = jsonObject.getString("uniqueID");
                                registrantModel.setUniqueId(Integer.parseInt(uniqueID));
//                                Log.e("@@@Record SAAHIL", "----Id  " + LeadIDTemp + "---");

                                lRegType = jsonObject.getString("lRegType");
                                registrantModel.setlRegType(lRegType);

                                registrantModel.setlEventID(eventID);

                                lGuestId = jsonObject.getString("lGuestID");
                                registrantModel.setlGuestID(lGuestId);

                                PrefixTemp = jsonObject.getString("sPrefix");
                                registrantModel.setsPrefix(PrefixTemp);

                                FirstNameTemp = jsonObject.getString("sFirstName");
                                registrantModel.setsFirstName(FirstNameTemp);

                                MiddleNameTemp = jsonObject.getString("sMiddleName");
                                registrantModel.setsMiddleName(MiddleNameTemp);

                                LastNameTemp = jsonObject.getString("sLastName");
                                registrantModel.setsLastName(LastNameTemp);

                                SuffixTemp = jsonObject.getString("sSuffix");
                                registrantModel.setsSuffix(SuffixTemp);

                                CredentialsTemp = jsonObject.getString("sCredentials");
                                registrantModel.setsCredentials(CredentialsTemp);

                                TitleTemp = jsonObject.getString("sTitle");
                                registrantModel.setsTitle(TitleTemp);

                                CompanyTemp = jsonObject.getString("sCompany");
                                registrantModel.setsCompany(CompanyTemp);

                                Address1Temp = jsonObject.getString("sAddress1");
                                registrantModel.setsAddress1(Address1Temp);

                                Address2Temp = jsonObject.getString("sAddress2");
                                registrantModel.setsAddress2(Address2Temp);

                                Address3Temp = jsonObject.getString("sAddress3");
                                registrantModel.setsAddress3(Address3Temp);

                                CityTemp = jsonObject.getString("sCity");
                                registrantModel.setsCity(CityTemp);

                                StateTemp = jsonObject.getString("sState");
                                registrantModel.setsState(StateTemp);

                                ZipTemp = jsonObject.getString("sZip");
                                registrantModel.setsZip(ZipTemp);

                                CountryTemp = jsonObject.getString("sCountry");
                                registrantModel.setsCountry(CountryTemp);

                                PhoneTemp = jsonObject.getString("sPhone");
                                registrantModel.setsPhone(PhoneTemp);

                                CellTemp = jsonObject.getString("sCell");
                                registrantModel.setsCell(CellTemp);

                                FaxTemp = jsonObject.getString("sFax");
                                registrantModel.setsFax(FaxTemp);

                                EmailTemp = jsonObject.getString("sEmail");
                                registrantModel.setsEmail(EmailTemp);

                                UpdatedOnTemp = jsonObject.getString("dtUpdatedOn");
                                registrantModel.setDtUpdatedOn(UpdatedOnTemp);

                                Other1Temp = jsonObject.getString("sOther1");
                                registrantModel.setsOther1(Other1Temp);

                                Other2Temp = jsonObject.getString("sOther2");
                                registrantModel.setsOther2(Other2Temp);

                                Other3Temp = jsonObject.getString("sOther3");
                                registrantModel.setsOther3(Other3Temp);
                                Other4Temp = jsonObject.getString("sOther4");
                                registrantModel.setsOther4(Other4Temp);
                                Other5Temp = jsonObject.getString("sOther5");
                                registrantModel.setsOther5(Other5Temp);
                                Other6Temp = jsonObject.getString("sOther6");
                                registrantModel.setsOther6(Other6Temp);
                                Other7Temp = jsonObject.getString("sOther7");
                                registrantModel.setsOther7(Other7Temp);
                                Other8Temp = jsonObject.getString("sOther8");
                                registrantModel.setsOther8(Other8Temp);
                                Other9Temp = jsonObject.getString("sOther9");
                                registrantModel.setsOther9(Other9Temp);
                                Other10Temp = jsonObject.getString("sOther10");
                                registrantModel.setsOther10(Other10Temp);
                                nStatus = jsonObject.getString("nStatus");
                                registrantModel.setnStatus(nStatus);

                                registrantModelList.add(registrantModel);

                                try {

                                    if (isRecordNeedToDelete(recordsList_temp, nStatus, LeadIDTemp)) {
                                        mainSQl = "DELETE FROM Registrants Where lLeadID =" + LeadIDTemp;
//                                        Log.e("@@@Record Del", "----Id  " + LeadIDTemp + "---" );
                                        statement = mainDB.compileStatement(mainSQl);
                                    } else {
                                        mainSQl = "INSERT OR REPLACE INTO `Registrants`(`lEventID`, `lLeadID`, `sPrefix`, `sFirstName`, `sMiddleName`, `sLastName`, `sSuffix`, `sCredentials`, `sTitle`, `sCompany`, `sAddress1`, `sAddress2`, `sAddress3`, `sCity`, `sState`, `sZip`, `sCountry`, `sPhone`, `sCell`, `sFax`, `sEmail`, `dtUpdatedOn`, sOther1, sOther2, sOther3, sOther4, sOther5, sOther6, sOther7, sOther8, sOther9, sOther10) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

//                                        Log.e("@@@Record Ins", "----Id  " + LeadIDTemp + "---");
                                        statement = mainDB.compileStatement(mainSQl);
                                        statement.bindLong(1, eventID);
                                        statement.bindString(2, LeadIDTemp);
                                        statement.bindString(3, PrefixTemp.replace("'", "`"));
                                        statement.bindString(4, FirstNameTemp.replace("'", "`"));
                                        statement.bindString(5, MiddleNameTemp.replace("'", "`"));
                                        statement.bindString(6, LastNameTemp.replace("'", "`"));
                                        statement.bindString(7, SuffixTemp.replace("'", "`"));
                                        statement.bindString(8, CredentialsTemp.replace("'", "`"));
                                        statement.bindString(9, TitleTemp.replace("'", "`"));
                                        statement.bindString(10, CompanyTemp.replace("'", "`"));
                                        statement.bindString(11, Address1Temp.replace("'", "`"));
                                        statement.bindString(12, Address2Temp.replace("'", "`"));
                                        statement.bindString(13, Address3Temp.replace("'", "`"));
                                        statement.bindString(14, CityTemp.replace("'", "`"));
                                        statement.bindString(15, StateTemp.replace("'", "`"));
                                        statement.bindString(16, ZipTemp.replace("'", "`"));
                                        statement.bindString(17, CountryTemp.replace("'", "`"));
                                        statement.bindString(18, PhoneTemp.replace("'", "`"));
                                        statement.bindString(19, CellTemp.replace("'", "`"));
                                        statement.bindString(20, FaxTemp.replace("'", "`"));
                                        statement.bindString(21, EmailTemp.replace("'", "`"));
                                        statement.bindString(22, UpdatedOnTemp);
                                        statement.bindString(23, Other1Temp.replace("'", "`"));
                                        statement.bindString(24, Other2Temp.replace("'", "`"));
                                        statement.bindString(25, Other3Temp.replace("'", "`"));
                                        statement.bindString(26, Other4Temp.replace("'", "`"));
                                        statement.bindString(27, Other5Temp.replace("'", "`"));
                                        statement.bindString(28, Other6Temp.replace("'", "`"));
                                        statement.bindString(29, Other7Temp.replace("'", "`"));
                                        statement.bindString(30, Other8Temp.replace("'", "`"));
                                        statement.bindString(31, Other9Temp.replace("'", "`"));
                                        statement.bindString(32, Other10Temp.replace("'", "`"));
                                        //LastSyncTemp = UpdatedOnTemp;
                                    }
                                    statement.execute();
                                    recordsList.clear();
                                    recordsList = getAllRegistrants(eventID);

                                } catch (android.database.sqlite.SQLiteException ex) {
                                    //   Log.e("@@@--- ex", "-----" + ex.toString() + ex.getMessage());
                                    appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
                                }
                            } catch (JSONException e) {
                                appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + e.toString());
                                return "failed";
                            }
                        }
                        saveRegistrantData(context, registrantModelList);

                        mainDB.setTransactionSuccessful();
                        mainDB.endTransaction();
                    } else {
                        bBatchDone = 1;
                    }

                } catch (Exception e) {
                    appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + e.toString());
                    return "failed";
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
            }

        } else {
            return "failed";
        }
        return "";
    }

    public String saveScanDataToOnlineScan(Context ctx, int eventID, int accountID, String deviceSerialNumber, int scanID, String scanLeadID, String scanUpdatedOn, int bPrinted) {
        if (isNetworkConnected(context) == true) {
            URL url;
            HttpURLConnection urlConnection = null;
            try {

                url = new URL("http://www.tslwebreg.com/tslcheckin/saveScanDataToOnlineScan.php");
                urlConnection = (HttpURLConnection) url
                        .openConnection();
                urlConnection.setReadTimeout(15000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));

                StringBuilder result = new StringBuilder();
                result.append(URLEncoder.encode("EventID", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(eventID), "UTF-8") + "&");
                result.append(URLEncoder.encode("AccountID", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(accountID), "UTF-8") + "&");
                result.append(URLEncoder.encode("DeviceSerialNumber", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(deviceSerialNumber), "UTF-8") + "&");
                result.append(URLEncoder.encode("ScanID", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(scanID), "UTF-8") + "&");
                result.append(URLEncoder.encode("LeadID", "UTF-8") + "=" + URLEncoder.encode(scanLeadID.toUpperCase(), "UTF-8") + "&");
                result.append(URLEncoder.encode("UpdatedOn", "UTF-8") + "=" + URLEncoder.encode(scanUpdatedOn, "UTF-8") + "&");
                result.append(URLEncoder.encode("Printed", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(bPrinted), "UTF-8"));

                writer.write(result.toString());

                writer.flush();
                writer.close();
                os.close();
                int responseCode = urlConnection.getResponseCode();

                String response = "";
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }
                    return "Success";

                } else {
                    return urlConnection.getResponseMessage();
                }
            } catch (Exception e) {
                appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + e.toString());
                return e.toString();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
        } else {
            return "No Internet";
        }
    }

    //here
    public boolean saveNewScan(Context ctx, int eventID, String deviceSerialNumber, String scanLeadID, int scanPrinted) {
        scanLeadID = scanLeadID.toUpperCase();

        //remove any non printable characters and trim the string
        scanLeadID = scanLeadID.replaceAll("\\p{C}", "?");
        scanLeadID = scanLeadID.trim();

        String scanLeadIDTemp = scanLeadID;

        //INSERT Scans
        String mainSQl = "INSERT INTO `Scans`(`lEventID`, `lLeadID`, `dtUpdatedOn`, bPrinted) VALUES (";
        mainSQl += eventID + ",";
        mainSQl += "'" + scanLeadIDTemp + "',";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateTime = sdf.format(new Date());
        mainSQl += "'" + currentDateTime + "',";
        mainSQl += scanPrinted + ")";
        try {
            mainDB.execSQL(mainSQl);
        } catch (android.database.sqlite.SQLiteException ex) {
            appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
        }

        //get last inserted row id
        Cursor cursor = mainDB.rawQuery("SELECT lScanID FROM Scans Where lEventID = " + eventID + " ORDER BY lScanID DESC LIMIT 1", null);
        int scanIDTemp = 0;
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                scanIDTemp = Integer.parseInt(cursor.getString(0));
                cursor.moveToNext();
            }
        }

        return true;
    }

    public boolean updateScansOnline(Context ctx, int eventID, int accountID, String deviceSerialNumber, int scanID, int updateAll) {
        String mainSQl = "Select dtLastSync from Events Where lEventID = " + eventID;
        Cursor cursor = mainDB.rawQuery(mainSQl, null);
        String scanLastSyncTemp = "";
        boolean bUpdateLastSync = true;

        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                scanLastSyncTemp = cursor.getString(0);
                cursor.moveToNext();
            }
        }
        cursor.close();

        mainSQl = "SELECT s.lScanID as lScanID, lLeadID as lLeadID, dtUpdatedOn, bPrinted FROM Scans as s Where s.lEventID = " + eventID;
        if (updateAll == 0) {
            mainSQl = mainSQl + " and dtUpdatedOn > '" + scanLastSyncTemp + "'";
        }
        if (scanID != 0) {
            mainSQl = mainSQl + " and lScanID = " + scanID;
        }
        mainSQl = mainSQl + " Order By dtUpdatedOn ASC ";
        Cursor cursor1 = mainDB.rawQuery(mainSQl, null);
        String lastUpdatedOn = "";

        if (cursor1.moveToFirst()) {
            while (cursor1.isAfterLast() == false) {
                if (bUpdateLastSync == true) {//we do not want to update online any other records if the previous one failed
                    int scanIDTemp = cursor1.getInt(0);
                    String scanLeadIDTemp = cursor1.getString(1);
                    String scanUpdatedOnTemp = cursor1.getString(2);
                    int scanPrintedTemp = cursor1.getInt(3);

                    String retValue1 = "";
                    retValue1 = saveScanDataToOnlineScan(ctx, eventID, accountID, devSerial, scanIDTemp, scanLeadIDTemp, scanUpdatedOnTemp, scanPrintedTemp);
                    if (retValue1 != "Success") {
                        bUpdateLastSync = false;
                        lastUpdatedOn = scanUpdatedOnTemp;
                    }

                    updateLastSync(0, eventID, scanUpdatedOnTemp);
                }

                cursor1.moveToNext();
            }
        }
        cursor1.close();

        if (lastUpdatedOn == "") {
            updateLastSync(0, eventID, "");
        }
        return true;
    }

    public boolean getEvents(Context ctx) {
        String mainSQl = "";
        String result = "";

        mainSQl = "Select * From Events ";
        Cursor cursor = mainDB.rawQuery(mainSQl, null);
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                int eventIDTemp = Integer.parseInt(cursor.getString(cursor.getColumnIndex("lEventID")));
                int eventAccountIDTemp = Integer.parseInt(cursor.getString(cursor.getColumnIndex("lAccountID")));

                //update event info
                result = updateEvent(eventIDTemp, eventAccountIDTemp);

                cursor.moveToNext();
            }
        }
        cursor.close();

        //for each event update Scans records
        cursor = mainDB.rawQuery(mainSQl, null);
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                int eventIDTemp = Integer.parseInt(cursor.getString(cursor.getColumnIndex("lEventID")));
                int eventAccountIDTemp = Integer.parseInt(cursor.getString(cursor.getColumnIndex("lAccountID")));
                String eventLastSync = cursor.getString(cursor.getColumnIndex("dtLastSync"));
                String returnVal2 = getAllRecordsFromOnline(context, eventIDTemp, eventAccountIDTemp, devSerial, 0, eventLastSync);
                updateScansOnline(ctx, eventIDTemp, eventAccountIDTemp, devSerial, 0, 0);

                cursor.moveToNext();
            }
        }
        cursor.close();

        return true;
    }

    public String updateEvent(int eventID, int eventAccountID) {
        String result = "";

        URL url;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL("http://www.tslwebreg.com/tslcheckin/getEventInfoForTSLCheckIn.php?event_id=" + eventID + "&user_id=" + eventAccountID);
            urlConnection = (HttpURLConnection) url
                    .openConnection();
            urlConnection.setReadTimeout(15000);
            urlConnection.setConnectTimeout(15000);
            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);
            if (in != null) {
                result = convertInputStreamToString(in);
            } else {
                return "issue";
            }
            try {
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0, count = jsonArray.length(); i < count; i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        //extract data
                        String eventNameTemp = jsonObject.getString("sName");
                        String eventLocationTemp = jsonObject.getString("sLocation");
                        String eventStartDateTemp = jsonObject.getString("dtStart");
                        String eventEndDateTemp = jsonObject.getString("dtEnd");
                        int eventAccountIDTemp = jsonObject.getInt("lAccountID");
                        String eventHeader1Temp = jsonObject.getString("sHeader1");
                        String eventHeader2Temp = jsonObject.getString("sHeader2");
                        String eventHeader3Temp = jsonObject.getString("sHeader3");
                        String eventHeader4Temp = jsonObject.getString("sHeader4");
                        String eventPictureTemp = jsonObject.getString("sPicture");
                        int eventLastNameSearchOnTemp = jsonObject.getInt("bLastNameSearchOn");
                        int eventEmailSearchOnTemp = jsonObject.getInt("bEmailSearchOn");
                        int eventScanOnTemp = jsonObject.getInt("bScanOn");

                        //update local db
                        String sql = "";
                        sql = "Update `Events` SET ";
                        sql += "sName = '" + eventNameTemp + "',";
                        sql += "sLocation = '" + eventLocationTemp + "',";
                        sql += "sStartDate = '" + eventStartDateTemp + "',";
                        sql += "sEndDate = '" + eventEndDateTemp + "',";
                        sql += "lAccountID = " + eventAccountIDTemp + ",";
                        sql += "sHeader1 = '" + eventHeader1Temp + "',";
                        sql += "sHeader2 = '" + eventHeader2Temp + "',";
                        sql += "sHeader3 = '" + eventHeader3Temp + "',";
                        sql += "sHeader4 = '" + eventHeader4Temp + "',";
                        sql += "sPicture = '" + eventPictureTemp + "',";
                        sql += "bLastNameSearchOn = " + eventLastNameSearchOnTemp + ",";
                        sql += "bEmailSearchOn = " + eventEmailSearchOnTemp + ",";
                        sql += "bScanOn = " + eventScanOnTemp;
                        sql += " Where lEventID = " + eventID;
                        try {
                            mainDB.execSQL(sql);
                        } catch (android.database.sqlite.SQLiteException ex) {
                            appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
                        }

                        //update array
                        for (int index = 0; index <= eventsList.size() - 1; index++) {
                            if (eventsList.get(index).get_eventID() == eventID) {
                                eventsList.get(index).set_eventName(eventNameTemp);
                                eventsList.get(index).set_eventLocation(eventLocationTemp);
                                eventsList.get(index).set_eventStart(eventStartDateTemp);
                                eventsList.get(index).set_eventEnd(eventEndDateTemp);
                                eventsList.get(index).set_eventAccountID(eventAccountIDTemp);
                                eventsList.get(index).set_eventHeader1(eventHeader1Temp);
                                eventsList.get(index).set_eventHeader2(eventHeader2Temp);
                                eventsList.get(index).set_eventHeader3(eventHeader3Temp);
                                eventsList.get(index).set_eventHeader4(eventHeader4Temp);
                                eventsList.get(index).set_eventPicture(eventPictureTemp);
                                eventsList.get(index).set_eventLastNameSearchOn(eventLastNameSearchOnTemp);
                                eventsList.get(index).set_eventEmailSearchOn(eventEmailSearchOnTemp);
                                eventsList.get(index).set_eventScanOn(eventScanOnTemp);
                            }
                        }

                    } catch (JSONException e) {
                        appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + e.toString());
                        return e.toString();
                    }
                }
            } catch (JSONException e) {
                appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + e.toString());
                return e.toString();
            }

        } catch (Exception e) {
            appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + e.toString());
            return e.toString();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return "success";
    }

    public String getRegistrants(int eventID, String leadID, String email, String lastName, String companyName) {
        String mainSQl = "";
        String result = "";

        regScanIDTemp = 0;
        regLeadIDTemp = "";
        regPrefixTemp = "";
        regFirstNameTemp = "";
        regMiddleNameTemp = "";
        regLastNameTemp = "";
        regSuffixTemp = "";
        regCredentialsTemp = "";
        regTitleTemp = "";
        regCompanyTemp = "";
        regAddress1Temp = "";
        regAddress2Temp = "";
        regAddress3Temp = "";
        regCityTemp = "";
        regStateTemp = "";
        regZipTemp = "";
        regCountryTemp = "";
        regPhoneTemp = "";
        regCellTemp = "";
        regFaxTemp = "";
        regEmailTemp = "";
        regUpdatedOnTemp = "";
        regPrinted = 1;
        regOther1Temp = "";
        regOther2Temp = "";
        regOther3Temp = "";
        regOther4Temp = "";
        regOther5Temp = "";
        regOther6Temp = "";
        regOther7Temp = "";
        regOther8Temp = "";
        regOther9Temp = "";
        regOther10Temp = "";

        if (leadID != "") {
            mainSQl = "Select * From Registrants Where lEventID = " + eventID + " and UPPER(lLeadID) = UPPER('" + leadID + "')";
        } else if (email != "") {
            mainSQl = "Select * From Registrants Where lEventID = " + eventID + " and UPPER(sEmail) = UPPER('" + email + "')";
        } else if (companyName != "") {
            //   mainSQl = "Select * From Registrants Where lEventID = " + eventID + " and UPPER(sCompany) = UPPER('" + companyName + "')";
            companyName = companyName.replace("'", "`");
            mainSQl = "Select * From Registrants Where lEventID = " + eventID + "  and sCompany  LIKE ?";
//                        mainSQl = "Select * From Registrants Where lEventID = " + eventID ;
        } else {
            lastName = lastName.replace("'", "`");
            mainSQl = "Select * From Registrants Where lEventID = " + eventID + "  and sLastName  LIKE ?";
        }
        mainSQl = mainSQl + " Order by sLastName, sFirstName, sCompany";
        recordsList.clear();
        try {
            Cursor cursor;
            if (leadID != "") {
                cursor = mainDB.rawQuery(mainSQl, null);
            } else if (email != "") {
                cursor = mainDB.rawQuery(mainSQl, null);
            } else if (companyName != "") {
                cursor = mainDB.rawQuery(mainSQl, new String[]{companyName + "%"});
            } else {
                cursor = mainDB.rawQuery(mainSQl, new String[]{lastName + "%"});
            }
//            cursor = mainDB.rawQuery(mainSQl, null);
            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    Record record = new Record();
                    record.set_ID(cursor.getInt(cursor.getColumnIndex("lLeadID")));
                    record.set_firstName(cursor.getString(cursor.getColumnIndex("sFirstName")));
                    record.set_lastName(cursor.getString(cursor.getColumnIndex("sLastName")));
                    record.set_company(cursor.getString(cursor.getColumnIndex("sCompany")));
                    recordsList.add(record);

                    cursor.moveToNext();

                }
            } else {
                result = "No record found.";
            }
            cursor.close();
        } catch (android.database.sqlite.SQLiteException ex) {
            appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
        }

        return result;
    }

    public String getRegistrantInfo(final int eventID, final String leadID, final int uniqueId) {
        String mainSQl = "";
        final String[] result = {""};

        regScanIDTemp = 0;
        regLeadIDTemp = "";
        regPrefixTemp = "";
        regFirstNameTemp = "";
        regMiddleNameTemp = "";
        regLastNameTemp = "";
        regSuffixTemp = "";
        regCredentialsTemp = "";
        regTitleTemp = "";
        regCompanyTemp = "";
        regAddress1Temp = "";
        regAddress2Temp = "";
        regAddress3Temp = "";
        regCityTemp = "";
        regStateTemp = "";
        regZipTemp = "";
        regCountryTemp = "";
        regPhoneTemp = "";
        regCellTemp = "";
        regFaxTemp = "";
        regEmailTemp = "";
        regUpdatedOnTemp = "";
        regPrinted = 1;
        regOther1Temp = "";
        regOther2Temp = "";
        regOther3Temp = "";
        regOther4Temp = "";
        regOther5Temp = "";
        regOther6Temp = "";
        regOther7Temp = "";
        regOther8Temp = "";
        regOther9Temp = "";
        regOther10Temp = "";

        new Thread(new Runnable() {
            @Override
            public void run() {

                registrantDatabase = RegistrantDatabase.getInstance(context);
                List<RegistrantModel> finalRegList = null;

                if (uniqueId != 0) {

                    finalRegList = registrantDatabase.getRegistrantDao().fetchRegistrantByEventAndleadIDAndUniqueId(eventID, leadID, "0", uniqueId);
                } else {
                    finalRegList = registrantDatabase.getRegistrantDao().fetchRegistrantByEventAndleadIDAndUniqueId(eventID, leadID, "0",Integer.parseInt(leadID));
                }

                if (finalRegList != null && finalRegList.size() > 0) {

                    recordsList.clear();

                    for (int i = 0; i < finalRegList.size(); i++) {

                        if (finalRegList.get(i).getnStatus().equals("0")) {

                            regLeadIDTemp = finalRegList.get(i).getlRegID();
                            regPrefixTemp = finalRegList.get(i).getsPrefix();
                            regFirstNameTemp  = finalRegList.get(i).getsFirstName();
                            regMiddleNameTemp = finalRegList.get(i).getsMiddleName();
                            regLastNameTemp   = finalRegList.get(i).getsLastName();
                            regSuffixTemp = finalRegList.get(i).getsSuffix();
                            regCredentialsTemp = finalRegList.get(i).getsCredentials();
                            regTitleTemp    = finalRegList.get(i).getsTitle();
                            regCompanyTemp  = finalRegList.get(i).getsCompany();
                            regAddress1Temp = finalRegList.get(i).getsAddress1();
                            regAddress2Temp = finalRegList.get(i).getsAddress2();
                            regAddress3Temp = finalRegList.get(i).getsAddress3();
                            regCityTemp     = finalRegList.get(i).getsCity();
                            regStateTemp    = finalRegList.get(i).getsState();
                            regZipTemp      = finalRegList.get(i).getsZip();
                            regCountryTemp  = finalRegList.get(i).getsCountry();
                            regPhoneTemp    = finalRegList.get(i).getsPhone();
                            regCellTemp     = finalRegList.get(i).getsCell();
                            regFaxTemp      = finalRegList.get(i).getsFax();
                            regEmailTemp    = finalRegList.get(i).getsEmail();
                            regOther1Temp   = finalRegList.get(i).getsOther1();
                            regOther2Temp   = finalRegList.get(i).getsOther2();
                            regOther3Temp   = finalRegList.get(i).getsOther3();
                            regOther4Temp   = finalRegList.get(i).getsOther4();
                            regOther5Temp   = finalRegList.get(i).getsOther5();
                            regOther6Temp   = finalRegList.get(i).getsOther6();
                            regOther7Temp   = finalRegList.get(i).getsOther7();
                            regOther8Temp   = finalRegList.get(i).getsOther8();
                            uniqueID        = finalRegList.get(i).getUniqueId();
                            regOther9Temp   = finalRegList.get(i).getsOther9();
                            regOther10Temp  = finalRegList.get(i).getsOther10();

                            Record record = new Record();
                            record.set_ID(Integer.parseInt(regLeadIDTemp));
                            record.set_firstName(regFirstNameTemp);
                            record.set_lastName(regLastNameTemp);
                            record.set_company(regCompanyTemp);
                            recordsList.add(record);
                        }
                    }

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String currentDateTime = sdf.format(new Date());
                    regUpdatedOnTemp = currentDateTime;

                    //add record to scan table
                    String sqlTemp = "INSERT INTO Scans (lEventID, lLeadID, dtUpdatedOn, bPrinted) VALUES (";
                    sqlTemp += eventID + ",";
                    sqlTemp += "'" + regLeadIDTemp + "',";
                    sqlTemp += "'" + regUpdatedOnTemp + "',";
                    sqlTemp += "0)";
                    try {
                        mainDB.execSQL(sqlTemp);
                    } catch (SQLiteException ex) {
                        appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
                    }

                    //get last inserted row id
                    Cursor cursor = mainDB.rawQuery("SELECT lScanID FROM Scans Where lEventID = " + eventID + " ORDER BY lScanID DESC LIMIT 1", null);
                    if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                            regScanIDTemp = Integer.parseInt(cursor.getString(0));
                            cursor.moveToNext();
                        }
                    }
                    cursor.close();
                } else {
                    result[0] = "No record found.";
                }
            }
        }).start();
//
//        mainSQl = "Select * From Registrants Where lEventID = " + eventID + " and UPPER(lLeadID) = UPPER('" + leadID + "')";
////Log.e("@@@ Main Sql","-----"+mainSQl);
//        Cursor cursor = mainDB.rawQuery(mainSQl, null);
//        if (cursor.moveToFirst()) {
//            while (cursor.isAfterLast() == false) {
//
//                regLeadIDTemp = cursor.getString(cursor.getColumnIndex("lLeadID"));
//                regPrefixTemp = cursor.getString(cursor.getColumnIndex("sPrefix"));
//                regFirstNameTemp = cursor.getString(cursor.getColumnIndex("sFirstName"));
//                regMiddleNameTemp = cursor.getString(cursor.getColumnIndex("sMiddleName"));
//                regLastNameTemp = cursor.getString(cursor.getColumnIndex("sLastName"));
//                regSuffixTemp = cursor.getString(cursor.getColumnIndex("sSuffix"));
//                regCredentialsTemp = cursor.getString(cursor.getColumnIndex("sCredentials"));
//                regTitleTemp = cursor.getString(cursor.getColumnIndex("sTitle"));
//                regCompanyTemp = cursor.getString(cursor.getColumnIndex("sCompany"));
//                regAddress1Temp = cursor.getString(cursor.getColumnIndex("sAddress1"));
//                regAddress2Temp = cursor.getString(cursor.getColumnIndex("sAddress2"));
//                regAddress3Temp = cursor.getString(cursor.getColumnIndex("sAddress3"));
//                regCityTemp = cursor.getString(cursor.getColumnIndex("sCity"));
//                regStateTemp = cursor.getString(cursor.getColumnIndex("sState"));
//                regZipTemp = cursor.getString(cursor.getColumnIndex("sZip"));
//                regCountryTemp = cursor.getString(cursor.getColumnIndex("sCountry"));
//                regPhoneTemp = cursor.getString(cursor.getColumnIndex("sPhone"));
//                regCellTemp = cursor.getString(cursor.getColumnIndex("sCell"));
//                regFaxTemp = cursor.getString(cursor.getColumnIndex("sFax"));
//                regEmailTemp = cursor.getString(cursor.getColumnIndex("sEmail"));
//                regOther1Temp = cursor.getString(cursor.getColumnIndex("sOther1"));
//                regOther2Temp = cursor.getString(cursor.getColumnIndex("sOther2"));
//                regOther3Temp = cursor.getString(cursor.getColumnIndex("sOther3"));
//                regOther4Temp = cursor.getString(cursor.getColumnIndex("sOther4"));
//                regOther5Temp = cursor.getString(cursor.getColumnIndex("sOther5"));
//                regOther6Temp = cursor.getString(cursor.getColumnIndex("sOther6"));
//                regOther7Temp = cursor.getString(cursor.getColumnIndex("sOther7"));
//                regOther8Temp = cursor.getString(cursor.getColumnIndex("sOther8"));
//                regOther9Temp = cursor.getString(cursor.getColumnIndex("sOther9"));
//                regOther10Temp = cursor.getString(cursor.getColumnIndex("sOther10"));
//
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                String currentDateTime = sdf.format(new Date());
//                regUpdatedOnTemp = currentDateTime;
//
//                //add record to scan table
//                String sqlTemp = "INSERT INTO Scans (lEventID, lLeadID, dtUpdatedOn, bPrinted) VALUES (";
//                sqlTemp += eventID + ",";
//                sqlTemp += "'" + regLeadIDTemp + "',";
//                sqlTemp += "'" + regUpdatedOnTemp + "',";
//                sqlTemp += "0)";
//                try {
//                    mainDB.execSQL(sqlTemp);
//                } catch (android.database.sqlite.SQLiteException ex) {
//                    appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
//                }
//
//                //get last inserted row id
//                cursor = mainDB.rawQuery("SELECT lScanID FROM Scans Where lEventID = " + eventID + " ORDER BY lScanID DESC LIMIT 1", null);
//                if (cursor.moveToFirst()) {
//                    while (!cursor.isAfterLast()) {
//                        regScanIDTemp = Integer.parseInt(cursor.getString(0));
//                        cursor.moveToNext();
//                    }
//                }
//                cursor.moveToNext();
//            }
//        } else {
//            result[0] = "No record found.";
//        }
//        cursor.close();

        return result[0];
    }


    public ArrayList<Record> getAllRegistrants(int eventID) {

        final ArrayList<Record> recordsList = new ArrayList<Record>();
        String mainSQl = "";

        mainSQl = "Select * From Registrants Where lEventID = " + eventID;


        recordsList.clear();
        try {
            Cursor cursor;

            cursor = mainDB.rawQuery(mainSQl, null);

            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {

//                    for(int i =0 ; i < registrantModels.size(); i++ ){

//                        if(registrantModels.get(i).getlRegID().equals(cursor.getColumnIndex("lLeadID"))){

//                            if(registrantModels.get(i).getlGuestID().equals("0") && registrantModels.get(i).getlRegType().equals("-1") ){

//                            }else{
                    Record record = new Record();
                    record.set_ID(cursor.getInt(cursor.getColumnIndex("lLeadID")));
                    record.set_firstName(cursor.getString(cursor.getColumnIndex("sFirstName")));
                    record.set_lastName(cursor.getString(cursor.getColumnIndex("sLastName")));
                    record.set_company(cursor.getString(cursor.getColumnIndex("sCompany")));
                    recordsList.add(record);
//                            }
//                        }
//                    }
                    cursor.moveToNext();
                }
            } else {
            }
            cursor.close();
        } catch (android.database.sqlite.SQLiteException ex) {
            appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
        }

        return recordsList;
    }

    private boolean isRecordNeedToDelete(ArrayList<Record> recordsList, String nStatus, String recordId) {

        if (nStatus.equals("0")) {
            return false;
        } else {

            boolean isPresent = true;
            /*for (int i = 0; i < recordsList.size(); i++) {

                // Log.e("@@ ids","--- recordsList.get(i).get_ID()"+recordsList.get(i).get_ID()+"----recordId"+recordId);
                if (recordsList.get(i).get_ID() == Integer.parseInt(recordId)) {
                    isPresent = true;
                    break;
                }
            }*/

            return isPresent;
        }


    }

}


