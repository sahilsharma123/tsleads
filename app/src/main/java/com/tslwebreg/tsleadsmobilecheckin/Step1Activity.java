package com.tslwebreg.tsleadsmobilecheckin;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Step1Activity extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;
    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    final Context context = this;
    private final Handler mHideHandler = new Handler();
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };
    Global global;
    boolean startingScan = false;
    private com.tslwebreg.tsleadsmobilecheckin.UIHelper helper = new com.tslwebreg.tsleadsmobilecheckin.UIHelper(this);
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public final static boolean isValidEmail(String target) {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (target.matches(emailPattern)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step1);

        verifyStoragePermissions(Step1Activity.this);

        global = ((Global) getApplicationContext());

        mVisible = true;
        mContentView = findViewById(R.id.step1_content);

        //set the headers and picture
        TextView txtTop = (TextView) findViewById(R.id.lblTop);
        txtTop.setText(global.eventsList.get(global._event_current_index).get_eventHeader1());
        TextView txtMiddle = (TextView) findViewById(R.id.lblMiddle);
        txtMiddle.setText(global.eventsList.get(global._event_current_index).get_eventHeader2());


        ImageView imageView1 = (ImageView) findViewById(R.id.imageView1);
        Glide.with(context)
                .load(global.eventsList.get(global._event_current_index).get_eventPicture()) // Remote URL of image.
                .error(R.drawable.sample_image)
                .into(imageView1);


//        ImageView imageView1 = (ImageView) findViewById(R.id.imageView1);
//        String filePicture = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + global.eventsList.get(global._event_current_index).get_eventPicture();
//        Drawable d = Drawable.createFromPath(filePicture);
//        imageView1.setImageDrawable(d);

        ImageView btnClose = (ImageView) findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Step1Activity.this.finish();
            }
        });


        //last name search
        if (global.eventsList.get(global._event_current_index).get_eventLastNameSearchOn() == 0) {
            findViewById(R.id.frameLastName).setVisibility(View.GONE);
        } else {
            final Button btnNextLast = (Button) findViewById(R.id.btnNextLast);
            final Button btnNextCompany_name = (Button) findViewById(R.id.btnNextCompany_name);
            final EditText txtLast = (EditText) findViewById(R.id.txtLastName);
            final EditText txtCompany_name = (EditText) findViewById(R.id.txtCompany_name);
            btnNextLast.setOnTouchListener(mDelayHideTouchListener);
            txtLast.setOnTouchListener(mDelayHideTouchListener);
            btnNextCompany_name.setOnTouchListener(mDelayHideTouchListener);
            txtCompany_name.setOnTouchListener(mDelayHideTouchListener);

            txtLast.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        getWindow().setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
                        );
                    }
                }
            });
            txtLast.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        btnNextLast.performClick();
                        handled = true;
                    }
                    return handled;
                }
            });
            txtCompany_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        getWindow().setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
                        );
                    }
                }
            });
            txtCompany_name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        btnNextCompany_name.performClick();
                        handled = true;
                    }
                    return handled;
                }
            });

            btnNextLast.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    global.isScan = false;

                    //verify if record exists
//                    if (global.getRegistrants(global.eventsList.get(global._event_current_index).get_eventID(), "", "", txtLast.getText().toString(), "") != "") {
//                        new AlertDialog.Builder(Step1Activity.this)
//                                .setTitle("Next Step")
//                                .setMessage("No records were found.")
//                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.cancel();
//                                    }
//                                })
//                                .setIcon(android.R.drawable.ic_dialog_alert)
//                                .show();
//                    } else

                    if (txtLast.length() < 2) {
                        new AlertDialog.Builder(Step1Activity.this)
                                .setTitle("Last Name")
                                .setMessage("Please enter at least 2 characters.")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        txtLast.requestFocus();
                        txtLast.setSelection(0, txtLast.length()-1);
                        return;
                    } else {
//                        txtLast.setText("");
                        Intent intent  = new Intent(context, Step2Activity.class);
                        intent.putExtra("lastname", txtLast.getText().toString());
                        txtLast.setText("");
                        startActivity(intent);
                    }
                }
            });
            btnNextCompany_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //verify if record exists
                    global.isScan = false;

//                    if (global.getRegistrants(global.eventsList.get(global._event_current_index).get_eventID(), "", "", "", txtCompany_name.getText().toString()) != "") {
//                        new AlertDialog.Builder(Step1Activity.this)
//                                .setTitle("Next Step")
//                                .setMessage("No records were found.")
//                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.cancel();
//                                    }
//                                })
//                                .setIcon(android.R.drawable.ic_dialog_alert)
//                                .show();
//                    } else
                    if (txtCompany_name.length() < 2) {
                        new AlertDialog.Builder(Step1Activity.this)
                                .setTitle("Last Name")
                                .setMessage("Please enter at least 2 characters.")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        txtCompany_name.requestFocus();
                        txtCompany_name.setSelection(0, txtCompany_name.length()-1);
                        return;
                    } else {
//                        txtCompany_name.setText("");
//                        startActivity(new Intent(context, Step2Activity.class));
                        Intent intent  = new Intent(context, Step2Activity.class);
                        intent.putExtra("companyname", txtCompany_name.getText().toString());
                        txtCompany_name.setText("");
                        startActivity(intent);
                    }
                }
            });
        }

        //Email search
        if (global.eventsList.get(global._event_current_index).get_eventEmailSearchOn() == 0) {
            findViewById(R.id.frameEmail).setVisibility(View.GONE);
        } else {
            findViewById(R.id.btnNextEmail).setOnTouchListener(mDelayHideTouchListener);
            findViewById(R.id.txtEmail).setOnTouchListener(mDelayHideTouchListener);
            final Button btnNextEmail = (Button) findViewById(R.id.btnNextEmail);
            final EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
            txtEmail.setCursorVisible(true);
            txtEmail.setSelectAllOnFocus(true);
            txtEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            txtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        getWindow().setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
                        );
                    }
                }
            });
            txtEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        btnNextEmail.performClick();
                        handled = true;
                    }
                    return handled;
                }
            });
            btnNextEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //verify if the email is valid
                    global.isScan = false;

                    txtEmail.setText(txtEmail.getText().toString().trim());
                    if (txtEmail.getText().length() == 0) {
                        new AlertDialog.Builder(Step1Activity.this)
                                .setTitle("Email")
                                .setMessage("Please enter an email.")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        txtEmail.requestFocus();
                        txtEmail.setSelection(0, txtEmail.length());
                        return;
                    }

                    String email = txtEmail.getText().toString().trim();
                    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                    if (!email.matches(emailPattern)) {
                        new AlertDialog.Builder(Step1Activity.this)
                                .setTitle("Email")
                                .setMessage("Please enter a valid email.")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        txtEmail.requestFocus();
                        txtEmail.setSelection(0, txtEmail.length());
                        return;
                    }

                    //verify if record exists
                    if (global.getRegistrants(global.eventsList.get(global._event_current_index).get_eventID(), "", txtEmail.getText().toString(), "", "") != "") {
                        new AlertDialog.Builder(Step1Activity.this)
                                .setTitle("Next Step")
                                .setMessage("No records were found.")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    } else {
                        txtEmail.setText("");
                        startActivity(new Intent(context, Step2Activity.class));
                    }
                }
            });
        }

        if (global.eventsList.get(global._event_current_index).get_eventScanOn() == 0) {
            findViewById(R.id.frameScan).setVisibility(View.GONE);
        } else {
            findViewById(R.id.btnScan).setOnTouchListener(mDelayHideTouchListener);
            Button btnScan = (Button) findViewById(R.id.btnScan);
            btnScan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startingScan = true;
                    Intent intent = new Intent("com.google.zxing.client.android3.SCAN");
                    intent.putExtra("com.google.zxing.client.android3.SCAN.SCAN_MODE", "QR_CODE_MODE,CODE_39,CODE_128");
                    intent.putExtra("com.google.zxing.client.android3.SCAN.RESULT_DISPLAY_DURATION_MS", 1L);
                    startActivityForResult(intent, 0);
                }
            });
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //hide();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
        );
    }

    @Override
    public void onBackPressed() {
        //this is the back button at the bottom of the device

        if (findViewById(R.id.frameLastName).getVisibility() == View.VISIBLE) {
            final EditText txtLast = (EditText) findViewById(R.id.txtLastName);
            txtLast.requestFocus();
            txtLast.setSelection(0, txtLast.length());
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(txtLast, InputMethodManager.SHOW_FORCED);
        } else if (findViewById(R.id.frameEmail).getVisibility() == View.VISIBLE) {
            final EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
            txtEmail.requestFocus();
            txtEmail.setSelection(0, txtEmail.length());
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(txtEmail, InputMethodManager.SHOW_FORCED);
        }
        return;
    }

    @Override
    public void onUserInteraction() {
        //we use this when the
        if (startingScan == false) {
            Intent intent = new Intent(context, Step1Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            intent.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            context.startActivity(intent);
        }
        //return;
    }

    @Override
    protected void onResume() {
        super.onResume();
        startingScan = false;

        if (findViewById(R.id.frameLastName).getVisibility() == View.VISIBLE) {
            EditText txtLast = (EditText) findViewById(R.id.txtLastName);
            txtLast.setText("");
        }
        if (findViewById(R.id.frameEmail).getVisibility() == View.VISIBLE) {
            EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
            txtEmail.setText("");
        }

        if (findViewById(R.id.frameLastName).getVisibility() == View.VISIBLE) {
            EditText txtLast = (EditText) findViewById(R.id.txtLastName);
            txtLast.requestFocus();
            txtLast.setSelection(0, txtLast.length());
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(txtLast, InputMethodManager.SHOW_FORCED);
        } else if (findViewById(R.id.frameEmail).getVisibility() == View.VISIBLE) {
            EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
            txtEmail.requestFocus();
            txtEmail.setSelection(0, txtEmail.length());
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(txtEmail, InputMethodManager.SHOW_FORCED);
        }

        hide();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {

                String contents = intent.getStringExtra("SCAN_RESULT");
                //check if scanned id exists in local Registrants table
                if (global.getRegistrantInfo(global.eventsList.get(global._event_current_index).get_eventID(), contents.toString(),0) != "") {
                    new AlertDialog.Builder(Step1Activity.this)
                            .setTitle("Scan")
                            .setMessage("No records were found.")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                } else {
                    // global.saveNewScan(context, global.eventsList.get(global._event_current_index).get_eventID(), global.devSerial, contents.toString(),0);
                    global.isScan = true;
                    global.lCurID = contents;
                    startActivity(new Intent(context, Step2Activity.class));
                }
            } else if (resultCode == RESULT_CANCELED) { // Handle cancel
                //Log.i("xZing", "Cancelled");
            }
        }
    }
}
