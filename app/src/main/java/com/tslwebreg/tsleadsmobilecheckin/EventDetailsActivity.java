package com.tslwebreg.tsleadsmobilecheckin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.content.Context;
import android.widget.CheckBox;
import android.widget.TextView;
import android.database.Cursor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class EventDetailsActivity extends AppCompatActivity {
    Global global;
    final Context context = this;
    int eventID = 0;
    int indexEvent = 0;

    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(com.tslwebreg.tsleadsmobilecheckin.R.layout.activity_event_details);
        Toolbar toolbar = (Toolbar) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.toolbar);
        setSupportActionBar(toolbar);

        global=((Global)getApplicationContext());
        indexEvent = global._event_current_index;
        eventID = global.eventsList.get(indexEvent).get_eventID();

        StrictMode.ThreadPolicy policy = new StrictMode.
                ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        int scansCount = 0;
        int leadsCount = 0;
        int scansNotFound = 0;
        int scansNotSynched = 0;

        //counts
        Cursor cursor = global.mainDB.rawQuery("SELECT count(lScanID) as count From Scans Where lEventID = " + eventID, null);
        if (cursor.moveToFirst()){
            while (!cursor.isAfterLast()){
                scansCount = scansCount + cursor.getInt(0);
                cursor.moveToNext();
            }
        }
        cursor.close();

        cursor = global.mainDB.rawQuery("SELECT count(DISTINCT lLeadID) as count From Scans Where lEventID = " + eventID + " Group By lEventID", null);
        if (cursor.moveToFirst()){
            while (!cursor.isAfterLast()){
                leadsCount = leadsCount + cursor.getInt(0);
                cursor.moveToNext();
            }
        }
        cursor.close();

        cursor = global.mainDB.rawQuery("SELECT count(lScanID) as count From Scans INNER JOIN Events ON Events.lEventID = Scans.lEventID Where Scans.lEventID = " + eventID + " and dtUpdatedOn > Events.dtLastSync", null);
        if (cursor.moveToFirst()){
            while (!cursor.isAfterLast()){
                scansNotSynched = scansNotSynched + cursor.getInt(0);
                cursor.moveToNext();
            }
        }
        cursor.close();

        ((TextView)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtScansCount)).setText(String.valueOf(scansCount));
        ((TextView)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtLeadsCount)).setText(String.valueOf(leadsCount));
        ((TextView)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtScansNotFound)).setText(String.valueOf(scansNotFound));
        ((TextView)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtScansNotSynched)).setText(String.valueOf(scansNotSynched));

        //event details
        ((TextView)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtEventName)).setText(global.eventsList.get(indexEvent).get_eventName());
        ((TextView)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtEventLocation)).setText(global.eventsList.get(indexEvent).get_eventLocation());
        ((TextView)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtEventStart)).setText(global.eventsList.get(indexEvent).get_eventStart());
        ((TextView)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtEventEnd)).setText(global.eventsList.get(indexEvent).get_eventEnd());
        ((TextView)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtDeviceSerialNumber)).setText(global.eventsList.get(indexEvent).get_deviceSerialnumber());
        ((TextView)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtDeviceName)).setText(global.devName);


        cursor = global.mainDB.rawQuery("SELECT dtLastSync FROM Events Where lEventID = " + eventID, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                ((TextView) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtLastSync)).setText(cursor.getString(0));
                cursor.moveToNext();
            }
        }
        cursor.close();


        Button btnReset=(Button)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.btnReset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtLastSync)).setText("2012-12-30 12:00:00");
                try{
                    global.mainDB.execSQL("Update Events set dtLastSync = '2012-12-30 12:00:00' Where lEventID = " + eventID);
                }catch(android.database.sqlite.SQLiteException ex){
                    global.appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
                }
            }
        });

        Button btnSubmitDB=(Button)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.btnSubmitDB);
        btnSubmitDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HttpURLConnection connection = null;
                DataOutputStream outputStream = null;
                DataInputStream inputStream = null;

                for (int ndo = 1; ndo < 3; ndo++) {
                    String pathToOurFile = "";
                    if (ndo == 1) {
                        pathToOurFile = "/data/user/0/com.tslwebreg.tsleadsmobilecheckin/databases/TSLCheckIn.db";
                    }
                    else{
                        pathToOurFile = "/data/user/0/com.tslwebreg.tsleadsmobilecheckin/TSLCheckinlog.file";
                    }
                    String urlServer = "http://www.tslwebreg.com/tslcheckin/handle_upload.php";
                    String lineEnd = "\r\n";
                    String twoHyphens = "--";
                    String boundary = "*****";

                    int bytesRead, bytesAvailable, bufferSize;
                    byte[] buffer;
                    int maxBufferSize = 1 * 1024 * 1024;

                    try {
                        FileInputStream fileInputStream = new FileInputStream(new File(pathToOurFile));

                        URL url = new URL(urlServer);
                        connection = (HttpURLConnection) url.openConnection();

                        // Allow Inputs &amp; Outputs.
                        connection.setDoInput(true);
                        connection.setDoOutput(true);
                        connection.setUseCaches(false);

                        // Set HTTP method to POST.
                        connection.setRequestMethod("POST");

                        connection.setRequestProperty("Connection", "Keep-Alive");
                        connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                        outputStream = new DataOutputStream(connection.getOutputStream());
                        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + pathToOurFile + "\"" + lineEnd);
                        outputStream.writeBytes(lineEnd);

                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];

                        // Read file
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                        while (bytesRead > 0) {
                            outputStream.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }

                        outputStream.writeBytes(lineEnd);
                        outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                        // Responses from the server (code and message)
                        int serverResponseCode = connection.getResponseCode();
                        String serverResponseMessage = connection.getResponseMessage();

                        fileInputStream.close();
                        outputStream.flush();
                        outputStream.close();
                    } catch (Exception ex) {
                        //Exception handling
                        global.appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
                    }
                }

                new AlertDialog.Builder(context)
                        .setTitle("Send database to Support")
                        .setMessage("The database was submitted to support.")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);

        final Button btnRemoveBadgeLayout = (Button) findViewById(R.id.btnRemoveBadgeLayout);
        btnRemoveBadgeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File fileCheck = new File(Environment.getExternalStorageDirectory() + File.separator + "CheckIn", "intermec_" + global.eventsList.get(global._event_current_index).get_eventID() + ".txt");
                if (fileCheck.exists()) {
                    fileCheck.delete();
                    new AlertDialog.Builder(context)
                            .setTitle("Remove Badge Layout")
                            .setMessage("The badge layout for this event was removed from this device.")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
                else{
                    new AlertDialog.Builder(context)
                            .setTitle("Remove Badge Layout")
                            .setMessage("No badge layout was found for this event on this device.")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }
        });

    }

    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            int scansNotSynched = 0;
            Cursor cursor = global.mainDB.rawQuery("SELECT count(lScanID) as count From Scans INNER JOIN Events ON Events.lEventID = Scans.lEventID Where Scans.lEventID = " + eventID + " and dtUpdatedOn > Events.dtLastSync ", null);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    scansNotSynched = scansNotSynched + cursor.getInt(0);
                    cursor.moveToNext();
                }
            }
            cursor.close();

            ((TextView) findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtScansNotSynched)).setText(String.valueOf(scansNotSynched));
            customHandler.postDelayed(this, 5000);
        }
    };

    public void onResume() {
        super.onResume();  // Always call the superclass method first
    }

    @Override
    public void onPause() {
        super.onPause();
        customHandler.removeCallbacks(updateTimerThread);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.tslwebreg.tsleadsmobilecheckin.R.menu.menu_event_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == com.tslwebreg.tsleadsmobilecheckin.R.id.miRemoveEvent) {
            int count1 = Integer.parseInt(((TextView)findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.txtScansNotSynched)).getText().toString());
            if (count1 != 0) {
                new AlertDialog.Builder(context)
                        .setTitle("Remove Event")
                        .setMessage("The event cannot be removed because there are scans that still need to be synced.")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
            else {
                //remove event
                new AlertDialog.Builder(context)
                        .setTitle("Remove Event")
                        .setMessage("Are you sure you want to remove this event? All data will be removed from this device.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    global.mainDB.execSQL("Delete from Scans Where lEventID = " + eventID);
                                } catch (android.database.sqlite.SQLiteException ex) {
                                    global.appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
                                }

                                try {
                                    global.mainDB.execSQL("Delete from Registrants Where lEventID = " + eventID);
                                } catch (android.database.sqlite.SQLiteException ex) {
                                    global.appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
                                }

                                try {
                                    global.mainDB.execSQL("Delete from Events Where lEventID = " + eventID);
                                } catch (android.database.sqlite.SQLiteException ex) {
                                    global.appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + ex.getMessage());
                                }

                                //remove item from list
                                global.eventsList.remove(indexEvent);
                                finish();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
