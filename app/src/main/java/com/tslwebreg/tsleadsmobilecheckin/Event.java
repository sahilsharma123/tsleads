package com.tslwebreg.tsleadsmobilecheckin;

/**
 * Created by fdemers on 12/23/2016.
 */
public class Event {

    private int _eventID;
    private String _eventName;
    private String _eventStart;
    private String _eventEnd;
    private String _eventLocation;
    private int _eventAccountID;
    private String _deviceSerialnumber;
    private String _eventLastSync;
    private String _eventHeader1;
    private String _eventHeader2;
    private String _eventHeader3;
    private String _eventHeader4;
    public String _eventPicture;
    public int _eventLastNameSearchOn;
    public int _eventEmailSearchOn;
    public int _eventScanOn;

    public Event(){

    }

    public Event(int eventID, String eventName,String eventStart, String eventEnd,String eventLocation, int eventTSLWebRegAccountID, String deviceSerialNumber, String eventLastSync, String eventHeader1, String eventHeader2, String eventHeader3, String eventHeader4, String eventPicture,int eventLastNameSearchOn, int eventEmailSearchOn, int eventScanOn){
        this._eventID = eventID;
        this._eventName = eventName;
        this._eventStart = eventStart;
        this._eventEnd = eventEnd;
        this._eventLocation = eventLocation;
        this._eventAccountID = eventTSLWebRegAccountID;
        this._deviceSerialnumber = deviceSerialNumber;
        this._eventLastSync = eventLastSync;
        this._eventHeader1 = eventHeader1;
        this._eventHeader2 = eventHeader2;
        this._eventHeader3 = eventHeader3;
        this._eventHeader4 = eventHeader4;
        this._eventPicture = eventPicture;
        this._eventLastNameSearchOn = eventLastNameSearchOn;
        this._eventEmailSearchOn = eventEmailSearchOn;
        this._eventScanOn = eventScanOn;
    }

    public void set_eventID(int eventID){
        this._eventID = eventID;
    }
    public void set_eventName(String eventName){
        this._eventName = eventName;
    }
    public void set_eventStart(String eventStart){
        this._eventStart = eventStart;
    }
    public void set_eventEnd(String eventEnd){
        this._eventEnd = eventEnd;
    }
    public void set_eventLocation(String eventLocation){
        this._eventLocation = eventLocation;
    }
    public void set_eventAccountID(int eventAccountID){
        this._eventAccountID = eventAccountID;
    }
    public void set_deviceSerialnumber(String deviceSerialnumber){
        this._deviceSerialnumber = deviceSerialnumber;
    }
    public void set_eventLastSync(String eventLastSync){
        this._eventLastSync = eventLastSync;
    }
    public void set_eventHeader1(String eventHeader1){
        this._eventHeader1 = eventHeader1;
    }
    public void set_eventHeader2(String eventHeader2){
        this._eventHeader2 = eventHeader2;
    }
    public void set_eventHeader3(String eventHeader3){
        this._eventHeader3 = eventHeader3;
    }
    public void set_eventHeader4(String eventHeader4){
        this._eventHeader4 = eventHeader4;
    }
    public void set_eventPicture(String eventPicture){
        this._eventPicture = eventPicture;
    }
    public void set_eventLastNameSearchOn(int eventLastNameSearchOn){
        this._eventLastNameSearchOn = eventLastNameSearchOn;
    }
    public void set_eventEmailSearchOn(int eventEmailSearchOn){
        this._eventEmailSearchOn = eventEmailSearchOn;
    }
    public void set_eventScanOn(int eventScanOn){
        this._eventScanOn = eventScanOn;
    }

    public int get_eventID(){
        return this._eventID;
    }
    public String get_eventName(){
        return this._eventName;
    }
    public String get_eventStart(){
        return this._eventStart;
    }
    public String get_eventEnd(){
        return this._eventEnd;
    }
    public String get_eventLocation(){
        return this._eventLocation;
    }
    public int get_eventAccountID(){
        return this._eventAccountID;
    }
    public String get_deviceSerialnumber(){
        return this._deviceSerialnumber;
    }
    public String get_eventLastSync(){
        return this._eventLastSync;
    }
    public String get_eventHeader1(){
        return this._eventHeader1;
    }
    public String get_eventHeader2(){
        return this._eventHeader2;
    }
    public String get_eventHeader3(){
        return this._eventHeader3;
    }
    public String get_eventHeader4(){
        return this._eventHeader4;
    }
    public String get_eventPicture(){
        return this._eventPicture;
    }
    public int get_eventLastNameSearchOn(){
        return this._eventLastNameSearchOn;
    }
    public int get_eventEmailSearchOn(){
        return this._eventEmailSearchOn;
    }
    public int get_eventScanOn(){
        return this._eventScanOn;
    }
}
