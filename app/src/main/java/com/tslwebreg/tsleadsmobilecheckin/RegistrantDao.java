package com.tslwebreg.tsleadsmobilecheckin;


import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

/**
 * Created by sahilsharma on 23/7/18.
 */

@Dao
public interface RegistrantDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMultipleCategories(List<RegistrantModel> registrantModels);

    @Update
    void updateMultipleCategories(List<RegistrantModel> registrantModels);

    @Query("SELECT * FROM RegistrantModel")
    List<RegistrantModel> fetchAll();

    @Query("SELECT * FROM RegistrantModel WHERE lEventID = :leventId and lRegID = :leadId and nStatus = :nStatus and uniqueID = :uniqueId")
    List<RegistrantModel> fetchRegistrantByEventAndleadIDAndUniqueId(int leventId, String leadId, String nStatus, int uniqueId);

    @Query("SELECT * FROM RegistrantModel WHERE lEventID = :leventId and uniqueID = :uniqueId and nStatus = :nStatus")
    List<RegistrantModel> fetchRegistrantByEventAndUniqueID(int leventId, String uniqueId, String nStatus);

    @Query("SELECT * FROM RegistrantModel WHERE lEventID = :leventId and lRegID = :leadId and nStatus = :nStatus")
    List<RegistrantModel> fetchRegistrantByEventAndleadID(int leventId, String leadId, String nStatus);

    @Query("SELECT * FROM RegistrantModel WHERE lEventID = :eventId and nStatus = :nStatus")
    List<RegistrantModel> fetchAll(int eventId, String nStatus);

    @Query("SELECT * FROM RegistrantModel WHERE lEventID = :leventId and sFirstName LIKE '%' || :firstname || '%' and sLastName LIKE '%' || :lastName || '%' and sCompany LIKE '%' || :companyname || '%' and lGuestID != :lguestId and lRegType != :regtype")
    List<RegistrantModel> getRegistrantsListOntheBasisofFields(int leventId, String firstname, String lastName, String companyname, String regtype, String lguestId);

    @Query("SELECT * FROM RegistrantModel WHERE lEventID = :leventId and sLastName LIKE :lastName || '%' ")
    List<RegistrantModel> getRegistrantsListOntheBasisofLastName(int leventId, String lastName);

    @Query("SELECT * FROM RegistrantModel WHERE lEventID = :leventId and sCompany LIKE '%' || :companyname || '%' ")
    List<RegistrantModel> getRegistrantsListOntheBasisofCompanyName(int leventId, String companyname);

}