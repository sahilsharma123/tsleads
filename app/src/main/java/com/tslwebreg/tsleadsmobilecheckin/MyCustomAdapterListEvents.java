package com.tslwebreg.tsleadsmobilecheckin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by fdemers on 12/23/2016.
 */
public class MyCustomAdapterListEvents extends BaseAdapter implements ListAdapter {
    private ArrayList<Event> list = new ArrayList<Event>();
    private Context context;
    Activity myActivity;
    Global global;

    public MyCustomAdapterListEvents(ArrayList<Event> list, Context context, Activity activity) {
        this.list = list;
        this.context = context;
        myActivity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return list.get(pos).get_eventID();//list.get(pos).getId();
        //just return 0 if your list items do not have an Id variable.
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(com.tslwebreg.tsleadsmobilecheckin.R.layout.my_custom_layout_with_info_btn, null);
        }

        //Handle TextView and display string from your list
        TextView listItemText = (TextView)view.findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.list_item_string);
        listItemText.setText(list.get(position).get_eventName());

        //Handle buttons and add onClickListeners
        Button infoBtn = (Button)view.findViewById(com.tslwebreg.tsleadsmobilecheckin.R.id.info_btn);

        infoBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                global=((Global)context.getApplicationContext());
                global._event_current_index = position;
                myActivity.startActivity(new Intent(context, EventDetailsActivity.class));
                notifyDataSetChanged();
            }
        });

        return view;
    }
}