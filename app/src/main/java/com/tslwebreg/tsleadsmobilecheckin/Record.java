package com.tslwebreg.tsleadsmobilecheckin;

/**
 * Created by fdemers on 2/2/2017.
 */

public class Record {
    private int _ID;
    private String _firstName;
    private String _guestId;
    private String _lastName;
    private String _company;

    public Record(){

    }

    public Record(int ID, String firstName, String lastName, String company){
        this._ID = ID;
        this._firstName = firstName;
        this._lastName = lastName;
        this._company = company;
    }

    public String get_guestId() {
        return _guestId;
    }

    public void set_guestId(String _guestId) {
        this._guestId = _guestId;
    }

    public void set_ID(int ID){
        this._ID = ID;
    }
    public void set_firstName(String firstName){
        this._firstName = firstName;
    }
    public void set_lastName(String lastName){
        this._lastName = lastName;
    }
    public void set_company(String company){
        this._company = company;
    }
    public int get_ID(){
        return this._ID;
    }
    public String get_firstName(){
        return this._firstName;
    }
    public String get_lastName(){
        return this._lastName;
    }
    public String get_company(){
        return this._company;
    }
}
