package com.tslwebreg.tsleadsmobilecheckin;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ListProgramsActivity extends AppCompatActivity implements View.OnClickListener, CheckBoxAdapter.checkboxselectlistener, RadioAdapter.radiochildclicklistener, EventAdapter.answercheckedlistener {

    private static String result = "";
    HashMap<String, RecordEvent> listMap = new HashMap<>();
    private RecyclerView recyclerView;
    private TextView nameText;
    private Button back, next;
    private ArrayList<RecordEvent> list = new ArrayList<>();
    private Global global;
    private String regId = "", EventID = "", lQuestionID = "", lAnswerID = "";
    private String selectedFirstname;
    private String selectedlastname;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_programs);
//allow strict mode
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        global = ((Global) getApplicationContext());

        recyclerView = (RecyclerView) findViewById(R.id.list_events);
        nameText = (TextView) findViewById(R.id.owner_name);
        back = (Button) findViewById(R.id.btnBack);
        next = (Button) findViewById(R.id.btnNext);

        back.setOnClickListener(this);

        ImageView imageView1 = (ImageView) findViewById(R.id.imageView1);
        Glide.with(this)
                .load(global.eventsList.get(global._event_current_index).get_eventPicture()) // Remote URL of image.
                .error(R.drawable.sample_image)
                .into(imageView1);

        if (getIntent().getExtras() != null) {

            if (getIntent().getExtras().containsKey("regId")) {

                regId = getIntent().getExtras().getString("regId");
            }

            if (getIntent().getExtras().containsKey("firstname")) {

                selectedFirstname = getIntent().getExtras().getString("firstname");
            }

            if (getIntent().getExtras().containsKey("lastname")) {

                selectedlastname = getIntent().getExtras().getString("lastname");
            }
        }

        nameText.setText(selectedFirstname +" "+ selectedlastname);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                getProgramList();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveAnswerStatusToServer();
            }
        });
    }

    public void saveAnswerStatusToServer() {

        List<RecordEvent> finalRecordEvents = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();

        //        if (!status.equals("")) {
//
        final ProgressDialog progress = new ProgressDialog(ListProgramsActivity.this);
        progress.setMessage("Sending Data");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.setProgress(0);
        progress.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        progress.show();


        Iterator it = listMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());

            RecordEvent recordEvent = (RecordEvent) pair.getValue();

            finalRecordEvents.add(recordEvent);

            if (recordEvent.getRequired().equals("1") && recordEvent.getlAnswerID() == null) {

                new AlertDialog.Builder(this)
                        .setTitle("Invalid Answer")
                        .setMessage("Please fill all answers!!")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return;
            }
        }

        String finalJsonString = "";

        for (int i = 0; i < finalRecordEvents.size(); i++) {

            try {
                JSONObject record = new JSONObject();
                record.put("accountId", finalRecordEvents.get(i).getlAccountID());
                record.put("eventId", finalRecordEvents.get(i).getlEventID());
                record.put("questionId", finalRecordEvents.get(i).getlQuestionID());
                record.put("regId", regId);
                record.put("answerId", finalRecordEvents.get(i).getCheckedAnswerId());

                if(finalRecordEvents.get(i).getlAnswerID() == null ){
                    record.put("answer", finalRecordEvents.get(i).getsAnswer());
                }else
                record.put("answer", "");

                jsonArray.put(record);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (jsonArray.length() > 0) {
            finalJsonString = jsonArray.toString();

        }

        URL url;
        HttpURLConnection urlConnection = null;
        try {

            url = new URL("http://www.tslwebreg.com/updateAnswerStatusForTSLCheckin.php");
            urlConnection = (HttpURLConnection) url
                    .openConnection();
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);

            OutputStream os = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));

            writer.write(finalJsonString);

            writer.flush();
            writer.close();
            os.close();
            int responseCode = urlConnection.getResponseCode();

            String response = "";
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    if (line != "") {
                        response += line;
                    }
                }
                JSONObject jsoResponse = new JSONObject(response);
                progress.dismiss();
                Toast.makeText(ListProgramsActivity.this, jsoResponse.optString("success",""), Toast.LENGTH_LONG).show();
                finish();

            } else {
                if (responseCode == 404)
                    Toast.makeText(ListProgramsActivity.this, responseCode + " Sever error occurs!!", Toast.LENGTH_LONG).show();
                return;
            }

        } catch (Exception e) {
            progress.dismiss();
            appendLog(Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "-" + e.toString());
            return;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    public void appendLog(String text) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateTime = sdf.format(new Date());
        String newText = currentDateTime + " " + text;

        File logFile = new File("/data/user/0/com.tslwebreg.tsleadsmobilecheckin/TSLCheckInlog.file");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(newText);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void getProgramList() {

        URL url;
        HttpURLConnection urlConnection = null;

        try {
            url = new URL("http://www.tslwebreg.com/getCustomQuestionsForTSLCheckIn.php?userID=" + global.eventsList.get(global._event_current_index).get_eventAccountID() + "&eventID=" + global.eventsList.get(global._event_current_index).get_eventID());
//            url = new URL("http://www.tslwebreg.com/getQuestionAnswerForTSLCheckin.php?userID=" + global.eventsList.get(global._event_current_index).get_eventAccountID() + "&eventID=" + global.eventsList.get(global._event_current_index).get_eventID()+"&regID="+ regId);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(10000);
            urlConnection.setRequestMethod("GET");
            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);
            if (in != null) {
                result = global.convertInputStreamToString(in);
            } else {
            }
            try {

                JSONObject jsonObject = new JSONObject(result);

                Iterator<String> keysItr = jsonObject.keys();

                String eventName = "";

                while (keysItr.hasNext()) {
                    String key = keysItr.next();
                    JSONObject innerJson = (JSONObject) jsonObject.get(key);

                    try {

                        String evenetId = innerJson.getString("lEventID");
                        String lAccountID = innerJson.getString("lAccountID");
                        String lAnswerID = innerJson.getString("lAnswerID");
                        String lQuestionID = innerJson.getString("lQuestionID");
                        String sAnswer = innerJson.getString("sAnswer");
                        String sCode = innerJson.getString("sCode");
                        String sName = innerJson.getString("sName");
                        eventName = sName;
                        String nType = innerJson.getString("nType");
                        String sAnswerStatus = innerJson.getString("sAnswerStatus");
                        String required = innerJson.getString("required");
                        String position = innerJson.getString("position");
                        String label = innerJson.getString("label");
                        String sType = innerJson.getString("sType");
                        String order = innerJson.getString("nOrder");
                        List<ModelAnswer> modelAnswerList = new ArrayList<>();

                        JSONArray answers = (JSONArray) innerJson.get("answerList");

                        for (int j = 0; j < answers.length(); j++) {

                            JSONObject jsonObject1 = answers.getJSONObject(j);
                            ModelAnswer modelAnswer = new ModelAnswer();
                            modelAnswer.setlAnswerID(jsonObject1.getString("lAnswerID"));
                            modelAnswer.setsAnswer(jsonObject1.getString("sAnswer"));

                            modelAnswerList.add(modelAnswer);
                        }

                        RecordEvent recordEvent = new RecordEvent();
                        recordEvent.setlEventID(evenetId);
                        recordEvent.setlAccountID(lAccountID);
                        recordEvent.setlAnswerID(lAnswerID);
                        recordEvent.setlQuestionID(lQuestionID);
                        recordEvent.setsAnswer(sAnswer);
                        recordEvent.setsCode(sCode);
                        recordEvent.setsName(sName);
                        recordEvent.setsAnswerStatus(sAnswerStatus);
                        recordEvent.setnType(nType);
                        recordEvent.setRequired(required);
                        recordEvent.setPosition(position);
                        recordEvent.setCheckedAnswerId(recordEvent.getlAnswerID());

                        recordEvent.setPosition(position);
                        recordEvent.setLabel(label);
                        recordEvent.setsType(sType);
                        recordEvent.setnOrder(order);
                        recordEvent.setModelAnswerList(modelAnswerList);
//                        templist.add(recordEvent);
                        list.add(recordEvent);

                        listMap.put(lQuestionID, recordEvent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Collections.sort(list, new Comparator<RecordEvent>() {
                    @Override
                    public int compare(RecordEvent recordEvent, RecordEvent t1) {
                        return recordEvent.getnOrder().compareTo(t1.getnOrder());
                    }
                });
                createList();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createList() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layoutManager);
        EventAdapter adapter = new EventAdapter(this, listMap, list, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnBack:
                finish();
        }
    }

    @Override
    public void onCheckboxClick(RecordEvent event, String answer) {
        listMap.put(event.getlQuestionID(), event);
    }

    @Override
    public void onRadioClick(RecordEvent event, String answer) {
    }

    @Override
    public void onclick(RecordEvent event, String answer) {
        listMap.put(event.getlQuestionID(), event);
    }

    public class RecordEvent {

        String checkedAnswerId;
        private String lEventID;
        private String lAccountID;
        private String lAnswerID;
        private String lQuestionID;
        private String sAnswer;
        private String sCode;
        private String sName;
        private String nType;
        private String sType;
        private String position;
        private String sAnswerStatus;
        private boolean isChecked;
        private String required;
        private String label;
        private String nOrder;
        private List<ModelAnswer> modelAnswerList;

        public RecordEvent() {

        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public String getsAnswerStatus() {
            return sAnswerStatus;
        }

        public void setsAnswerStatus(String sAnswerStatus) {
            this.sAnswerStatus = sAnswerStatus;
        }

        public String getlEventID() {
            return lEventID;
        }

        public void setlEventID(String lEventID) {
            this.lEventID = lEventID;
        }

        public String getlAccountID() {
            return lAccountID;
        }

        public void setlAccountID(String lAccountID) {
            this.lAccountID = lAccountID;
        }

        public String getlAnswerID() {
            return lAnswerID;
        }

        public void setlAnswerID(String lAnswerID) {
            this.lAnswerID = lAnswerID;
        }

        public String getlQuestionID() {
            return lQuestionID;
        }

        public void setlQuestionID(String lQuestionID) {
            this.lQuestionID = lQuestionID;
        }

        public String getsAnswer() {
            return sAnswer;
        }

        public void setsAnswer(String sAnswer) {
            this.sAnswer = sAnswer;
        }

        public String getsCode() {
            return sCode;
        }

        public void setsCode(String sCode) {
            this.sCode = sCode;
        }

        public String getsName() {
            return sName;
        }

        public void setsName(String sName) {
            this.sName = sName;
        }

        public String getnType() {
            return nType;
        }

        public void setnType(String nType) {
            this.nType = nType;
        }

        public String getsType() {
            return sType;
        }

        public void setsType(String sType) {
            this.sType = sType;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getRequired() {
            return required;
        }

        public void setRequired(String required) {
            this.required = required;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getnOrder() {
            return nOrder;
        }

        public void setnOrder(String nOrder) {
            this.nOrder = nOrder;
        }

        public List<ModelAnswer> getModelAnswerList() {
            return modelAnswerList;
        }

        public void setModelAnswerList(List<ModelAnswer> modelAnswerList) {
            this.modelAnswerList = modelAnswerList;
        }

        public String getCheckedAnswerId() {
            if(checkedAnswerId==null)
                checkedAnswerId="";
            return checkedAnswerId;
        }

        public void setCheckedAnswerId(String checkedAnswerId) {
            this.checkedAnswerId = checkedAnswerId;
        }
    }
}